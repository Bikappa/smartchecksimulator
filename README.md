La cartella 'relazione' contiene il report finale del progetto


Materiale utile
=======================================

Guide su OpenSceneGraph e cartella scansioni
https://www.dropbox.com/sh/iwipxk8guam3p5h/AAAR9VF8xYFQoKuLsmCsJaL4a?dl=0

Link pagina della fotocamera dello scanner (probabile)
http://www.photonfocus.com/products/camerafinder/camera/?no_cache=1&prid=60


Cartella con screenshot scansioni: https://www.dropbox.com/sh/iwipxk8guam3p5h/AAAR9VF8xYFQoKuLsmCsJaL4a?dl=0
NB: SCANSIONI FATTE SULLE MESH NUOVE (salvate in Dati2/ invece che Dati/)