/****************************************************************************
** Meta object code from reading C++ file 'ScanProcess.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../itr/ScanProcess.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ScanProcess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_itr__ScanProcess_t {
    QByteArrayData data[8];
    char stringdata0[91];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_itr__ScanProcess_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_itr__ScanProcess_t qt_meta_stringdata_itr__ScanProcess = {
    {
QT_MOC_LITERAL(0, 0, 16), // "itr::ScanProcess"
QT_MOC_LITERAL(1, 17, 14), // "progressUpdate"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 8), // "progress"
QT_MOC_LITERAL(4, 42, 19), // "progressDescription"
QT_MOC_LITERAL(5, 62, 11), // "stateUpdate"
QT_MOC_LITERAL(6, 74, 8), // "previous"
QT_MOC_LITERAL(7, 83, 7) // "current"

    },
    "itr::ScanProcess\0progressUpdate\0\0"
    "progress\0progressDescription\0stateUpdate\0"
    "previous\0current"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_itr__ScanProcess[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   24,    2, 0x06 /* Public */,
       5,    2,   29,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double, QMetaType::QString,    3,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    6,    7,

       0        // eod
};

void itr::ScanProcess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ScanProcess *_t = static_cast<ScanProcess *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->progressUpdate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->stateUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ScanProcess::*_t)(double , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScanProcess::progressUpdate)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ScanProcess::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScanProcess::stateUpdate)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject itr::ScanProcess::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_itr__ScanProcess.data,
      qt_meta_data_itr__ScanProcess,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *itr::ScanProcess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *itr::ScanProcess::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_itr__ScanProcess.stringdata0))
        return static_cast<void*>(const_cast< ScanProcess*>(this));
    return QThread::qt_metacast(_clname);
}

int itr::ScanProcess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void itr::ScanProcess::progressUpdate(double _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void itr::ScanProcess::stateUpdate(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
struct qt_meta_stringdata_itr__ProgressWatcherThread_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_itr__ProgressWatcherThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_itr__ProgressWatcherThread_t qt_meta_stringdata_itr__ProgressWatcherThread = {
    {
QT_MOC_LITERAL(0, 0, 26) // "itr::ProgressWatcherThread"

    },
    "itr::ProgressWatcherThread"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_itr__ProgressWatcherThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void itr::ProgressWatcherThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject itr::ProgressWatcherThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_itr__ProgressWatcherThread.data,
      qt_meta_data_itr__ProgressWatcherThread,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *itr::ProgressWatcherThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *itr::ProgressWatcherThread::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_itr__ProgressWatcherThread.stringdata0))
        return static_cast<void*>(const_cast< ProgressWatcherThread*>(this));
    return QThread::qt_metacast(_clname);
}

int itr::ProgressWatcherThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
