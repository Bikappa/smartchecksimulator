/****************************************************************************
** Meta object code from reading C++ file 'configureview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../configureview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'configureview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FileDialogSceneario_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FileDialogSceneario_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FileDialogSceneario_t qt_meta_stringdata_FileDialogSceneario = {
    {
QT_MOC_LITERAL(0, 0, 19) // "FileDialogSceneario"

    },
    "FileDialogSceneario"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FileDialogSceneario[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FileDialogSceneario::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject FileDialogSceneario::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileDialogSceneario.data,
      qt_meta_data_FileDialogSceneario,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FileDialogSceneario::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FileDialogSceneario::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FileDialogSceneario.stringdata0))
        return static_cast<void*>(const_cast< FileDialogSceneario*>(this));
    return QObject::qt_metacast(_clname);
}

int FileDialogSceneario::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_ConfigureView_t {
    QByteArrayData data[11];
    char stringdata0[130];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ConfigureView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ConfigureView_t qt_meta_stringdata_ConfigureView = {
    {
QT_MOC_LITERAL(0, 0, 13), // "ConfigureView"
QT_MOC_LITERAL(1, 14, 18), // "showFileChooserFor"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 12), // "workerUpdate"
QT_MOC_LITERAL(4, 47, 8), // "progress"
QT_MOC_LITERAL(5, 56, 5), // "start"
QT_MOC_LITERAL(6, 62, 16), // "onProgressUpdate"
QT_MOC_LITERAL(7, 79, 19), // "progressDescription"
QT_MOC_LITERAL(8, 99, 13), // "onStateUpdate"
QT_MOC_LITERAL(9, 113, 8), // "previous"
QT_MOC_LITERAL(10, 122, 7) // "current"

    },
    "ConfigureView\0showFileChooserFor\0\0"
    "workerUpdate\0progress\0start\0"
    "onProgressUpdate\0progressDescription\0"
    "onStateUpdate\0previous\0current"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ConfigureView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x0a /* Public */,
       3,    1,   42,    2, 0x0a /* Public */,
       5,    0,   45,    2, 0x0a /* Public */,
       6,    2,   46,    2, 0x0a /* Public */,
       8,    2,   51,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Float,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double, QMetaType::QString,    4,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    9,   10,

 // enums: name, flags, count, data

 // enum data: key, value

       0        // eod
};

void ConfigureView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ConfigureView *_t = static_cast<ConfigureView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->showFileChooserFor((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->workerUpdate((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->start(); break;
        case 3: _t->onProgressUpdate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->onStateUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject ConfigureView::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ConfigureView.data,
      qt_meta_data_ConfigureView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ConfigureView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ConfigureView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ConfigureView.stringdata0))
        return static_cast<void*>(const_cast< ConfigureView*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ConfigureView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
