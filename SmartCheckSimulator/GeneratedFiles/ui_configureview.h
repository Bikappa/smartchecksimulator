/********************************************************************************
** Form generated from reading UI file 'configureview.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGUREVIEW_H
#define UI_CONFIGUREVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigureViewClass
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox_10;
    QProgressBar *progressBar;
    QLabel *info_label;
    QPushButton *run_btn;
    QGroupBox *configuration_control_block;
    QGroupBox *ChooseFileBox;
    QLabel *label_2;
    QPushButton *load_configuration_btn;
    QPushButton *save_configuration_btn;
    QLineEdit *configuration_path;
    QGroupBox *groupBox;
    QGroupBox *ChooseFileBox_2;
    QLabel *label_3;
    QPushButton *choose_model_file_btn;
    QLineEdit *model_path;
    QGroupBox *camera_parameters_group;
    QGroupBox *groupBox_3;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *fundamental_0;
    QLineEdit *fundamental_2;
    QLineEdit *fundamental_3;
    QLineEdit *fundamental_1;
    QLineEdit *fundamental_5;
    QLineEdit *fundamental_4;
    QLineEdit *fundamental_6;
    QLineEdit *fundamental_7;
    QLineEdit *fundamental_8;
    QLabel *label_4;
    QGroupBox *groupBox_4;
    QLineEdit *distortion_coefficients_4;
    QLineEdit *distortion_coefficients_3;
    QLineEdit *distortion_coefficients_1;
    QLineEdit *distortion_coefficients_0;
    QLineEdit *distortion_coefficients_2;
    QGroupBox *groupBox_6;
    QLabel *label_9;
    QLineEdit *sensor_width;
    QLineEdit *sensor_height;
    QLabel *label_8;
    QGroupBox *groupBox_7;
    QLineEdit *focal_length;
    QLabel *label_13;
    QGroupBox *groupBox_11;
    QLineEdit *camera_fps;
    QLabel *label_33;
    QGroupBox *scanner_parameters_group;
    QGroupBox *groupBox_8;
    QLineEdit *lasers_distance;
    QLineEdit *lasers_aperture;
    QLineEdit *lasers_angle;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_34;
    QLineEdit *brightness_threshold;
    QComboBox *scan_mode;
    QLabel *scan_mode_label;
    QGroupBox *groupBox_9;
    QLineEdit *starting_position_0;
    QLabel *label_15;
    QLabel *label_16;
    QLineEdit *starting_position_1;
    QLineEdit *starting_position_2;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QLineEdit *direction_0;
    QLineEdit *direction_2;
    QLineEdit *direction_1;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLineEdit *roi_0;
    QLineEdit *roi_1;
    QLabel *label_28;
    QLabel *label_30;
    QLineEdit *scansion_length;
    QLineEdit *speed;
    QLabel *label_31;
    QLabel *label_29;
    QLabel *label_32;
    QLineEdit *roi_3;
    QLabel *label_26;
    QLineEdit *roi_2;
    QLabel *label_27;
    QLineEdit *looking_2;
    QLineEdit *looking_1;
    QLabel *label_55;
    QLineEdit *looking_0;
    QLabel *label_35;
    QLabel *label_36;
    QLabel *label_37;
    QPushButton *save_cloud;

    void setupUi(QMainWindow *ConfigureViewClass)
    {
        if (ConfigureViewClass->objectName().isEmpty())
            ConfigureViewClass->setObjectName(QStringLiteral("ConfigureViewClass"));
        ConfigureViewClass->resize(600, 700);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ConfigureViewClass->sizePolicy().hasHeightForWidth());
        ConfigureViewClass->setSizePolicy(sizePolicy);
        ConfigureViewClass->setMinimumSize(QSize(600, 700));
        ConfigureViewClass->setMaximumSize(QSize(600, 700));
        ConfigureViewClass->setContextMenuPolicy(Qt::NoContextMenu);
        ConfigureViewClass->setWindowTitle(QStringLiteral("SmartCheckSimilator"));
        ConfigureViewClass->setTabShape(QTabWidget::Rounded);
        centralWidget = new QWidget(ConfigureViewClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        centralWidget->setContextMenuPolicy(Qt::NoContextMenu);
        centralWidget->setStyleSheet(QStringLiteral(""));
        groupBox_10 = new QGroupBox(centralWidget);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        groupBox_10->setGeometry(QRect(0, 580, 601, 81));
        progressBar = new QProgressBar(groupBox_10);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(10, 20, 581, 23));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy1);
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignCenter);
        info_label = new QLabel(groupBox_10);
        info_label->setObjectName(QStringLiteral("info_label"));
        info_label->setGeometry(QRect(16, 60, 571, 20));
        info_label->setAlignment(Qt::AlignCenter);
        run_btn = new QPushButton(centralWidget);
        run_btn->setObjectName(QStringLiteral("run_btn"));
        run_btn->setGeometry(QRect(180, 660, 111, 31));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 128, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(170, 255, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(127, 255, 63, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(42, 127, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(56, 170, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        QBrush brush6(QColor(0, 0, 0, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush7(QColor(255, 255, 220, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush6);
        QBrush brush8(QColor(128, 128, 128, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        QBrush brush9(QColor(211, 211, 211, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush6);
        QBrush brush10(QColor(85, 255, 0, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush10);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush6);
        run_btn->setPalette(palette);
        run_btn->setStyleSheet(QLatin1String("QPushButton{\n"
"	background-color: green;\n"
"	color: white;\n"
"}\n"
"\n"
"QPushButton:!enabled{\n"
"	background-color: Lightgrey;\n"
"	color: gray;\n"
"}"));
        configuration_control_block = new QGroupBox(centralWidget);
        configuration_control_block->setObjectName(QStringLiteral("configuration_control_block"));
        configuration_control_block->setEnabled(true);
        configuration_control_block->setGeometry(QRect(0, 0, 601, 571));
        ChooseFileBox = new QGroupBox(configuration_control_block);
        ChooseFileBox->setObjectName(QStringLiteral("ChooseFileBox"));
        ChooseFileBox->setEnabled(true);
        ChooseFileBox->setGeometry(QRect(0, 0, 611, 71));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(ChooseFileBox->sizePolicy().hasHeightForWidth());
        ChooseFileBox->setSizePolicy(sizePolicy2);
        ChooseFileBox->setStyleSheet(QLatin1String("QGroupBox {\n"
"	border: none;\n"
"}\n"
""));
        label_2 = new QLabel(ChooseFileBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 10, 161, 16));
        label_2->setStyleSheet(QLatin1String("QGroupBox {	\n"
"border: none;\n"
"}"));
        load_configuration_btn = new QPushButton(ChooseFileBox);
        load_configuration_btn->setObjectName(QStringLiteral("load_configuration_btn"));
        load_configuration_btn->setGeometry(QRect(480, 30, 111, 31));
        load_configuration_btn->setStyleSheet(QLatin1String("QPushButton{\n"
"background-color: green;\n"
"color: white;\n"
"}\n"
"\n"
"QPushButton:!enabled{\n"
"	 background-color: Lightgrey;\n"
"	 color: gray;\n"
"}"));
        save_configuration_btn = new QPushButton(ChooseFileBox);
        save_configuration_btn->setObjectName(QStringLiteral("save_configuration_btn"));
        save_configuration_btn->setEnabled(true);
        save_configuration_btn->setGeometry(QRect(360, 30, 111, 31));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush11(QColor(255, 165, 0, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush6);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush6);
        save_configuration_btn->setPalette(palette1);
        save_configuration_btn->setStyleSheet(QLatin1String("QPushButton{\n"
"	background-color: orange;\n"
"	color: white;\n"
"}\n"
"\n"
"\n"
"QPushButton:!enabled{\n"
"	 background-color: Lightgrey;\n"
"	 color: gray;\n"
"}\n"
""));
        configuration_path = new QLineEdit(ChooseFileBox);
        configuration_path->setObjectName(QStringLiteral("configuration_path"));
        configuration_path->setGeometry(QRect(20, 30, 331, 31));
        groupBox = new QGroupBox(configuration_control_block);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 70, 601, 511));
        ChooseFileBox_2 = new QGroupBox(groupBox);
        ChooseFileBox_2->setObjectName(QStringLiteral("ChooseFileBox_2"));
        ChooseFileBox_2->setGeometry(QRect(0, 20, 600, 61));
        sizePolicy2.setHeightForWidth(ChooseFileBox_2->sizePolicy().hasHeightForWidth());
        ChooseFileBox_2->setSizePolicy(sizePolicy2);
        ChooseFileBox_2->setStyleSheet(QLatin1String("QGroupBox {	\n"
"border: none;\n"
"}"));
        label_3 = new QLabel(ChooseFileBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 0, 161, 16));
        choose_model_file_btn = new QPushButton(ChooseFileBox_2);
        choose_model_file_btn->setObjectName(QStringLiteral("choose_model_file_btn"));
        choose_model_file_btn->setEnabled(true);
        choose_model_file_btn->setGeometry(QRect(480, 20, 111, 31));
        model_path = new QLineEdit(ChooseFileBox_2);
        model_path->setObjectName(QStringLiteral("model_path"));
        model_path->setGeometry(QRect(20, 20, 451, 31));
        camera_parameters_group = new QGroupBox(groupBox);
        camera_parameters_group->setObjectName(QStringLiteral("camera_parameters_group"));
        camera_parameters_group->setGeometry(QRect(0, 80, 601, 181));
        camera_parameters_group->setStyleSheet(QLatin1String("QGroupBox#camera_parameters_group {	\n"
"border: none;\n"
"}"));
        groupBox_3 = new QGroupBox(camera_parameters_group);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 20, 201, 151));
        groupBox_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        gridLayoutWidget = new QWidget(groupBox_3);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 10, 201, 141));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        fundamental_0 = new QLineEdit(gridLayoutWidget);
        fundamental_0->setObjectName(QStringLiteral("fundamental_0"));
        sizePolicy.setHeightForWidth(fundamental_0->sizePolicy().hasHeightForWidth());
        fundamental_0->setSizePolicy(sizePolicy);
        fundamental_0->setMinimumSize(QSize(50, 20));
        fundamental_0->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_0, 2, 0, 1, 1);

        fundamental_2 = new QLineEdit(gridLayoutWidget);
        fundamental_2->setObjectName(QStringLiteral("fundamental_2"));
        sizePolicy.setHeightForWidth(fundamental_2->sizePolicy().hasHeightForWidth());
        fundamental_2->setSizePolicy(sizePolicy);
        fundamental_2->setMinimumSize(QSize(50, 20));
        fundamental_2->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_2, 2, 2, 1, 1);

        fundamental_3 = new QLineEdit(gridLayoutWidget);
        fundamental_3->setObjectName(QStringLiteral("fundamental_3"));
        sizePolicy.setHeightForWidth(fundamental_3->sizePolicy().hasHeightForWidth());
        fundamental_3->setSizePolicy(sizePolicy);
        fundamental_3->setMinimumSize(QSize(50, 20));
        fundamental_3->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_3, 3, 0, 1, 1);

        fundamental_1 = new QLineEdit(gridLayoutWidget);
        fundamental_1->setObjectName(QStringLiteral("fundamental_1"));
        sizePolicy.setHeightForWidth(fundamental_1->sizePolicy().hasHeightForWidth());
        fundamental_1->setSizePolicy(sizePolicy);
        fundamental_1->setMinimumSize(QSize(50, 20));
        fundamental_1->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_1, 2, 1, 1, 1);

        fundamental_5 = new QLineEdit(gridLayoutWidget);
        fundamental_5->setObjectName(QStringLiteral("fundamental_5"));
        sizePolicy.setHeightForWidth(fundamental_5->sizePolicy().hasHeightForWidth());
        fundamental_5->setSizePolicy(sizePolicy);
        fundamental_5->setMinimumSize(QSize(50, 20));
        fundamental_5->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_5, 3, 2, 1, 1);

        fundamental_4 = new QLineEdit(gridLayoutWidget);
        fundamental_4->setObjectName(QStringLiteral("fundamental_4"));
        sizePolicy.setHeightForWidth(fundamental_4->sizePolicy().hasHeightForWidth());
        fundamental_4->setSizePolicy(sizePolicy);
        fundamental_4->setMinimumSize(QSize(50, 20));
        fundamental_4->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_4, 3, 1, 1, 1);

        fundamental_6 = new QLineEdit(gridLayoutWidget);
        fundamental_6->setObjectName(QStringLiteral("fundamental_6"));
        sizePolicy.setHeightForWidth(fundamental_6->sizePolicy().hasHeightForWidth());
        fundamental_6->setSizePolicy(sizePolicy);
        fundamental_6->setMinimumSize(QSize(50, 20));
        fundamental_6->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_6, 4, 0, 1, 1);

        fundamental_7 = new QLineEdit(gridLayoutWidget);
        fundamental_7->setObjectName(QStringLiteral("fundamental_7"));
        sizePolicy.setHeightForWidth(fundamental_7->sizePolicy().hasHeightForWidth());
        fundamental_7->setSizePolicy(sizePolicy);
        fundamental_7->setMinimumSize(QSize(50, 20));
        fundamental_7->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_7, 4, 1, 1, 1);

        fundamental_8 = new QLineEdit(gridLayoutWidget);
        fundamental_8->setObjectName(QStringLiteral("fundamental_8"));
        sizePolicy.setHeightForWidth(fundamental_8->sizePolicy().hasHeightForWidth());
        fundamental_8->setSizePolicy(sizePolicy);
        fundamental_8->setMinimumSize(QSize(50, 20));
        fundamental_8->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(fundamental_8, 4, 2, 1, 1);

        label_4 = new QLabel(camera_parameters_group);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(230, 20, 111, 16));
        groupBox_4 = new QGroupBox(camera_parameters_group);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(220, 20, 371, 71));
        distortion_coefficients_4 = new QLineEdit(groupBox_4);
        distortion_coefficients_4->setObjectName(QStringLiteral("distortion_coefficients_4"));
        distortion_coefficients_4->setGeometry(QRect(290, 30, 61, 20));
        distortion_coefficients_3 = new QLineEdit(groupBox_4);
        distortion_coefficients_3->setObjectName(QStringLiteral("distortion_coefficients_3"));
        distortion_coefficients_3->setGeometry(QRect(220, 30, 61, 20));
        distortion_coefficients_1 = new QLineEdit(groupBox_4);
        distortion_coefficients_1->setObjectName(QStringLiteral("distortion_coefficients_1"));
        distortion_coefficients_1->setGeometry(QRect(80, 30, 61, 20));
        distortion_coefficients_0 = new QLineEdit(groupBox_4);
        distortion_coefficients_0->setObjectName(QStringLiteral("distortion_coefficients_0"));
        distortion_coefficients_0->setGeometry(QRect(10, 30, 61, 20));
        distortion_coefficients_2 = new QLineEdit(groupBox_4);
        distortion_coefficients_2->setObjectName(QStringLiteral("distortion_coefficients_2"));
        distortion_coefficients_2->setGeometry(QRect(150, 30, 61, 20));
        groupBox_6 = new QGroupBox(camera_parameters_group);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setGeometry(QRect(220, 90, 171, 80));
        label_9 = new QLabel(groupBox_6);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(90, 50, 71, 16));
        sensor_width = new QLineEdit(groupBox_6);
        sensor_width->setObjectName(QStringLiteral("sensor_width"));
        sensor_width->setGeometry(QRect(20, 30, 61, 20));
        sensor_height = new QLineEdit(groupBox_6);
        sensor_height->setObjectName(QStringLiteral("sensor_height"));
        sensor_height->setGeometry(QRect(90, 30, 61, 20));
        label_8 = new QLabel(groupBox_6);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 50, 51, 16));
        groupBox_7 = new QGroupBox(camera_parameters_group);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setGeometry(QRect(400, 90, 91, 80));
        focal_length = new QLineEdit(groupBox_7);
        focal_length->setObjectName(QStringLiteral("focal_length"));
        focal_length->setEnabled(true);
        focal_length->setGeometry(QRect(10, 30, 71, 20));
        label_13 = new QLabel(groupBox_7);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(30, 50, 31, 16));
        groupBox_11 = new QGroupBox(camera_parameters_group);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        groupBox_11->setGeometry(QRect(500, 90, 91, 80));
        camera_fps = new QLineEdit(groupBox_11);
        camera_fps->setObjectName(QStringLiteral("camera_fps"));
        camera_fps->setEnabled(true);
        camera_fps->setGeometry(QRect(10, 30, 71, 20));
        label_33 = new QLabel(groupBox_11);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setGeometry(QRect(30, 50, 31, 16));
        scanner_parameters_group = new QGroupBox(groupBox);
        scanner_parameters_group->setObjectName(QStringLiteral("scanner_parameters_group"));
        scanner_parameters_group->setGeometry(QRect(0, 270, 591, 231));
        scanner_parameters_group->setStyleSheet(QLatin1String("QGroupBox#scanner_parameters_group{	\n"
"border: none;\n"
"}"));
        groupBox_8 = new QGroupBox(scanner_parameters_group);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setGeometry(QRect(10, 0, 181, 231));
        lasers_distance = new QLineEdit(groupBox_8);
        lasers_distance->setObjectName(QStringLiteral("lasers_distance"));
        lasers_distance->setGeometry(QRect(20, 40, 41, 20));
        lasers_aperture = new QLineEdit(groupBox_8);
        lasers_aperture->setObjectName(QStringLiteral("lasers_aperture"));
        lasers_aperture->setGeometry(QRect(110, 40, 41, 20));
        lasers_angle = new QLineEdit(groupBox_8);
        lasers_angle->setObjectName(QStringLiteral("lasers_angle"));
        lasers_angle->setGeometry(QRect(20, 120, 41, 20));
        label = new QLabel(groupBox_8);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 20, 61, 21));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_5 = new QLabel(groupBox_8);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(90, 20, 61, 21));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_10 = new QLabel(groupBox_8);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(-10, 100, 61, 21));
        label_10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_11 = new QLabel(groupBox_8);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(20, 60, 47, 20));
        label_11->setAlignment(Qt::AlignCenter);
        label_12 = new QLabel(groupBox_8);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(110, 60, 47, 20));
        label_12->setAlignment(Qt::AlignCenter);
        label_14 = new QLabel(groupBox_8);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(20, 140, 47, 20));
        label_14->setAlignment(Qt::AlignCenter);
        label_34 = new QLabel(groupBox_8);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setGeometry(QRect(100, 100, 81, 16));
        brightness_threshold = new QLineEdit(groupBox_8);
        brightness_threshold->setObjectName(QStringLiteral("brightness_threshold"));
        brightness_threshold->setGeometry(QRect(110, 120, 41, 20));
        scan_mode = new QComboBox(groupBox_8);
        scan_mode->setObjectName(QStringLiteral("scan_mode"));
        scan_mode->setGeometry(QRect(20, 190, 141, 22));
        scan_mode_label = new QLabel(groupBox_8);
        scan_mode_label->setObjectName(QStringLiteral("scan_mode_label"));
        scan_mode_label->setGeometry(QRect(10, 170, 61, 21));
        scan_mode_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        groupBox_9 = new QGroupBox(scanner_parameters_group);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setGeometry(QRect(200, 0, 391, 231));
        starting_position_0 = new QLineEdit(groupBox_9);
        starting_position_0->setObjectName(QStringLiteral("starting_position_0"));
        starting_position_0->setGeometry(QRect(40, 50, 31, 20));
        label_15 = new QLabel(groupBox_9);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(27, 50, 16, 16));
        label_16 = new QLabel(groupBox_9);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(77, 50, 16, 16));
        starting_position_1 = new QLineEdit(groupBox_9);
        starting_position_1->setObjectName(QStringLiteral("starting_position_1"));
        starting_position_1->setGeometry(QRect(90, 50, 31, 20));
        starting_position_2 = new QLineEdit(groupBox_9);
        starting_position_2->setObjectName(QStringLiteral("starting_position_2"));
        starting_position_2->setGeometry(QRect(140, 50, 31, 20));
        label_17 = new QLabel(groupBox_9);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(127, 50, 16, 16));
        label_18 = new QLabel(groupBox_9);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(310, 60, 16, 20));
        label_19 = new QLabel(groupBox_9);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(210, 60, 16, 20));
        label_20 = new QLabel(groupBox_9);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setGeometry(QRect(260, 60, 16, 20));
        direction_0 = new QLineEdit(groupBox_9);
        direction_0->setObjectName(QStringLiteral("direction_0"));
        direction_0->setEnabled(true);
        direction_0->setGeometry(QRect(220, 60, 31, 20));
        direction_2 = new QLineEdit(groupBox_9);
        direction_2->setObjectName(QStringLiteral("direction_2"));
        direction_2->setEnabled(true);
        direction_2->setGeometry(QRect(319, 60, 31, 20));
        direction_1 = new QLineEdit(groupBox_9);
        direction_1->setObjectName(QStringLiteral("direction_1"));
        direction_1->setEnabled(true);
        direction_1->setGeometry(QRect(270, 60, 31, 20));
        label_21 = new QLabel(groupBox_9);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setGeometry(QRect(27, 30, 121, 16));
        label_22 = new QLabel(groupBox_9);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setGeometry(QRect(206, 40, 121, 16));
        label_23 = new QLabel(groupBox_9);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setGeometry(QRect(70, 180, 51, 16));
        label_24 = new QLabel(groupBox_9);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setGeometry(QRect(10, 160, 41, 16));
        label_25 = new QLabel(groupBox_9);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setGeometry(QRect(10, 180, 61, 16));
        roi_0 = new QLineEdit(groupBox_9);
        roi_0->setObjectName(QStringLiteral("roi_0"));
        roi_0->setGeometry(QRect(10, 200, 31, 20));
        roi_1 = new QLineEdit(groupBox_9);
        roi_1->setObjectName(QStringLiteral("roi_1"));
        roi_1->setGeometry(QRect(70, 200, 31, 20));
        label_28 = new QLabel(groupBox_9);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setGeometry(QRect(150, 160, 41, 16));
        label_30 = new QLabel(groupBox_9);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setGeometry(QRect(20, 90, 61, 16));
        scansion_length = new QLineEdit(groupBox_9);
        scansion_length->setObjectName(QStringLiteral("scansion_length"));
        scansion_length->setGeometry(QRect(20, 110, 51, 20));
        speed = new QLineEdit(groupBox_9);
        speed->setObjectName(QStringLiteral("speed"));
        speed->setGeometry(QRect(90, 110, 51, 20));
        label_31 = new QLabel(groupBox_9);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setGeometry(QRect(90, 90, 51, 16));
        label_29 = new QLabel(groupBox_9);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setGeometry(QRect(30, 130, 31, 16));
        label_32 = new QLabel(groupBox_9);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setGeometry(QRect(100, 130, 41, 16));
        roi_3 = new QLineEdit(groupBox_9);
        roi_3->setObjectName(QStringLiteral("roi_3"));
        roi_3->setGeometry(QRect(210, 200, 31, 20));
        label_26 = new QLabel(groupBox_9);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setGeometry(QRect(150, 180, 61, 16));
        roi_2 = new QLineEdit(groupBox_9);
        roi_2->setObjectName(QStringLiteral("roi_2"));
        roi_2->setGeometry(QRect(150, 200, 31, 20));
        label_27 = new QLabel(groupBox_9);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setGeometry(QRect(210, 180, 51, 16));
        looking_2 = new QLineEdit(groupBox_9);
        looking_2->setObjectName(QStringLiteral("looking_2"));
        looking_2->setEnabled(true);
        looking_2->setGeometry(QRect(320, 110, 31, 20));
        looking_1 = new QLineEdit(groupBox_9);
        looking_1->setObjectName(QStringLiteral("looking_1"));
        looking_1->setEnabled(true);
        looking_1->setGeometry(QRect(270, 110, 31, 20));
        label_55 = new QLabel(groupBox_9);
        label_55->setObjectName(QStringLiteral("label_55"));
        label_55->setGeometry(QRect(210, 90, 121, 16));
        looking_0 = new QLineEdit(groupBox_9);
        looking_0->setObjectName(QStringLiteral("looking_0"));
        looking_0->setEnabled(true);
        looking_0->setGeometry(QRect(220, 110, 31, 20));
        label_35 = new QLabel(groupBox_9);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setGeometry(QRect(210, 110, 16, 20));
        label_36 = new QLabel(groupBox_9);
        label_36->setObjectName(QStringLiteral("label_36"));
        label_36->setGeometry(QRect(310, 110, 16, 20));
        label_37 = new QLabel(groupBox_9);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setGeometry(QRect(260, 110, 16, 20));
        save_cloud = new QPushButton(centralWidget);
        save_cloud->setObjectName(QStringLiteral("save_cloud"));
        save_cloud->setEnabled(false);
        save_cloud->setGeometry(QRect(310, 660, 111, 31));
        ConfigureViewClass->setCentralWidget(centralWidget);
        QWidget::setTabOrder(configuration_path, save_configuration_btn);
        QWidget::setTabOrder(save_configuration_btn, load_configuration_btn);
        QWidget::setTabOrder(load_configuration_btn, model_path);
        QWidget::setTabOrder(model_path, choose_model_file_btn);
        QWidget::setTabOrder(choose_model_file_btn, fundamental_0);
        QWidget::setTabOrder(fundamental_0, fundamental_1);
        QWidget::setTabOrder(fundamental_1, fundamental_2);
        QWidget::setTabOrder(fundamental_2, fundamental_3);
        QWidget::setTabOrder(fundamental_3, fundamental_4);
        QWidget::setTabOrder(fundamental_4, fundamental_5);
        QWidget::setTabOrder(fundamental_5, fundamental_6);
        QWidget::setTabOrder(fundamental_6, fundamental_7);
        QWidget::setTabOrder(fundamental_7, fundamental_8);
        QWidget::setTabOrder(fundamental_8, distortion_coefficients_0);
        QWidget::setTabOrder(distortion_coefficients_0, distortion_coefficients_1);
        QWidget::setTabOrder(distortion_coefficients_1, distortion_coefficients_2);
        QWidget::setTabOrder(distortion_coefficients_2, distortion_coefficients_3);
        QWidget::setTabOrder(distortion_coefficients_3, distortion_coefficients_4);
        QWidget::setTabOrder(distortion_coefficients_4, sensor_width);
        QWidget::setTabOrder(sensor_width, sensor_height);
        QWidget::setTabOrder(sensor_height, focal_length);
        QWidget::setTabOrder(focal_length, camera_fps);
        QWidget::setTabOrder(camera_fps, lasers_distance);
        QWidget::setTabOrder(lasers_distance, lasers_aperture);
        QWidget::setTabOrder(lasers_aperture, lasers_angle);
        QWidget::setTabOrder(lasers_angle, brightness_threshold);
        QWidget::setTabOrder(brightness_threshold, scan_mode);
        QWidget::setTabOrder(scan_mode, starting_position_0);
        QWidget::setTabOrder(starting_position_0, starting_position_1);
        QWidget::setTabOrder(starting_position_1, starting_position_2);
        QWidget::setTabOrder(starting_position_2, direction_0);
        QWidget::setTabOrder(direction_0, direction_1);
        QWidget::setTabOrder(direction_1, direction_2);
        QWidget::setTabOrder(direction_2, looking_0);
        QWidget::setTabOrder(looking_0, looking_1);
        QWidget::setTabOrder(looking_1, looking_2);
        QWidget::setTabOrder(looking_2, scansion_length);
        QWidget::setTabOrder(scansion_length, speed);
        QWidget::setTabOrder(speed, roi_0);
        QWidget::setTabOrder(roi_0, roi_1);
        QWidget::setTabOrder(roi_1, roi_2);
        QWidget::setTabOrder(roi_2, roi_3);
        QWidget::setTabOrder(roi_3, run_btn);

        retranslateUi(ConfigureViewClass);
        QObject::connect(run_btn, SIGNAL(clicked()), ConfigureViewClass, SLOT(start()));

        QMetaObject::connectSlotsByName(ConfigureViewClass);
    } // setupUi

    void retranslateUi(QMainWindow *ConfigureViewClass)
    {
        groupBox_10->setTitle(QString());
        info_label->setText(QString());
        run_btn->setText(QApplication::translate("ConfigureViewClass", "Run", 0));
        configuration_control_block->setTitle(QString());
        ChooseFileBox->setTitle(QString());
        label_2->setText(QApplication::translate("ConfigureViewClass", "Load settings from file:", 0));
        load_configuration_btn->setText(QApplication::translate("ConfigureViewClass", "Load", 0));
        save_configuration_btn->setText(QApplication::translate("ConfigureViewClass", "Save", 0));
        groupBox->setTitle(QApplication::translate("ConfigureViewClass", "Settings", 0));
        ChooseFileBox_2->setTitle(QString());
        label_3->setText(QApplication::translate("ConfigureViewClass", "Model file", 0));
        choose_model_file_btn->setText(QApplication::translate("ConfigureViewClass", "Choose File", 0));
        camera_parameters_group->setTitle(QString());
        groupBox_3->setTitle(QApplication::translate("ConfigureViewClass", "Camera matrix", 0));
        label_4->setText(QString());
        groupBox_4->setTitle(QApplication::translate("ConfigureViewClass", "Distortion Coefficients", 0));
        groupBox_6->setTitle(QApplication::translate("ConfigureViewClass", "Sensor size", 0));
        label_9->setText(QApplication::translate("ConfigureViewClass", "height (px)", 0));
        label_8->setText(QApplication::translate("ConfigureViewClass", "width (px)", 0));
        groupBox_7->setTitle(QApplication::translate("ConfigureViewClass", "Focal length", 0));
        focal_length->setText(QString());
        label_13->setText(QApplication::translate("ConfigureViewClass", "(mm)", 0));
        groupBox_11->setTitle(QApplication::translate("ConfigureViewClass", "Framerate", 0));
        camera_fps->setText(QString());
        label_33->setText(QApplication::translate("ConfigureViewClass", "(fps)", 0));
        scanner_parameters_group->setTitle(QString());
        groupBox_8->setTitle(QApplication::translate("ConfigureViewClass", "Lasers", 0));
        label->setText(QApplication::translate("ConfigureViewClass", "Distance:", 0));
        label_5->setText(QApplication::translate("ConfigureViewClass", "Aperture:", 0));
        label_10->setText(QApplication::translate("ConfigureViewClass", "Angle:  ", 0));
        label_11->setText(QApplication::translate("ConfigureViewClass", "(mm)", 0));
        label_12->setText(QApplication::translate("ConfigureViewClass", "(degrees)", 0));
        label_14->setText(QApplication::translate("ConfigureViewClass", "(degrees)", 0));
        label_34->setText(QApplication::translate("ConfigureViewClass", "Light threshold", 0));
        brightness_threshold->setText(QString());
        scan_mode->clear();
        scan_mode->insertItems(0, QStringList()
         << QApplication::translate("ConfigureViewClass", "Model under laser cross", 0)
         << QApplication::translate("ConfigureViewClass", "Model over laser cross", 0)
        );
        scan_mode_label->setText(QApplication::translate("ConfigureViewClass", "Scan mode:", 0));
        groupBox_9->setTitle(QApplication::translate("ConfigureViewClass", "Other parameters", 0));
        label_15->setText(QApplication::translate("ConfigureViewClass", "X:", 0));
        label_16->setText(QApplication::translate("ConfigureViewClass", "Y:", 0));
        label_17->setText(QApplication::translate("ConfigureViewClass", "Z:", 0));
        label_18->setText(QApplication::translate("ConfigureViewClass", "Z:", 0));
        label_19->setText(QApplication::translate("ConfigureViewClass", "X:", 0));
        label_20->setText(QApplication::translate("ConfigureViewClass", "Y:", 0));
        label_21->setText(QApplication::translate("ConfigureViewClass", "Starting position", 0));
        label_22->setText(QApplication::translate("ConfigureViewClass", "Direction", 0));
        label_23->setText(QApplication::translate("ConfigureViewClass", "height:", 0));
        label_24->setText(QApplication::translate("ConfigureViewClass", "ROI 1", 0));
        label_25->setText(QApplication::translate("ConfigureViewClass", "start row:", 0));
        label_28->setText(QApplication::translate("ConfigureViewClass", "ROI 2", 0));
        label_30->setText(QApplication::translate("ConfigureViewClass", "Scan length:", 0));
        label_31->setText(QApplication::translate("ConfigureViewClass", "Speed:", 0));
        label_29->setText(QApplication::translate("ConfigureViewClass", "(mm)", 0));
        label_32->setText(QApplication::translate("ConfigureViewClass", "(mm/s)", 0));
        label_26->setText(QApplication::translate("ConfigureViewClass", "start row:", 0));
        label_27->setText(QApplication::translate("ConfigureViewClass", "height:", 0));
        label_55->setText(QApplication::translate("ConfigureViewClass", "Looking", 0));
        label_35->setText(QApplication::translate("ConfigureViewClass", "X:", 0));
        label_36->setText(QApplication::translate("ConfigureViewClass", "Z:", 0));
        label_37->setText(QApplication::translate("ConfigureViewClass", "Y:", 0));
        save_cloud->setText(QApplication::translate("ConfigureViewClass", "Save cloud", 0));
        Q_UNUSED(ConfigureViewClass);
    } // retranslateUi

};

namespace Ui {
    class ConfigureViewClass: public Ui_ConfigureViewClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGUREVIEW_H
