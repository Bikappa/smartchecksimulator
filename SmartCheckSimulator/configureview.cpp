#include "configureview.h"

ConfigureView::ConfigureView(QWidget *parent)
	: QMainWindow(parent)
{

	ui.setupUi(this);

	QSignalMapper *signalMapper = new QSignalMapper(this);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(showFileChooserFor(int)));

	signalMapper->setMapping(this->ui.load_configuration_btn, FileDialogSceneario::scenario::OPEN_CONFIGURATION);
	signalMapper->setMapping(this->ui.save_configuration_btn, FileDialogSceneario::scenario::SAVE_CONFIGURATION);
	signalMapper->setMapping(this->ui.choose_model_file_btn, FileDialogSceneario::scenario::OPEN_MODEL_FILE);
	signalMapper->setMapping(this->ui.save_cloud, FileDialogSceneario::scenario::SAVE_PCL);

	connect(this->ui.load_configuration_btn, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(this->ui.save_configuration_btn, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(this->ui.choose_model_file_btn, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(this->ui.save_cloud, SIGNAL(clicked()), signalMapper, SLOT(map()));

	for (int e = 0; e < 9; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("fundamental_" + std::to_string(e)))
			->setValidator(new QDoubleValidator(-INFINITY, +INFINITY, 2));
	}

	for (int e = 0; e < 5; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("distortion_coefficients_" + std::to_string(e)))
			->setValidator(new QDoubleValidator(-INFINITY, +INFINITY, 2));
	}

	ui.sensor_width->setValidator(new QIntValidator(0, 100000));
	ui.sensor_height->setValidator(new QIntValidator(0, 100000));
	ui.camera_fps->setValidator(new QDoubleValidator(1.0, +INFINITY, 2));

	ui.focal_length->setValidator(new QDoubleValidator(0.0, 10000, 2));
	ui.lasers_distance->setValidator(new QDoubleValidator(0.0, 10000, 2));
	ui.lasers_angle->setValidator(new QDoubleValidator(0.0, 90, 2));
	ui.lasers_aperture->setValidator(new QDoubleValidator(0.0, 180, 2));

	ui.brightness_threshold->setValidator(new QDoubleValidator(0.0, 1.0, 2));
	ui.scansion_length->setValidator(new QDoubleValidator(0.0, +INFINITY, 2));
	ui.speed->setValidator(new QDoubleValidator(0.0, +INFINITY, 2));


	for (int e = 0; e < 4; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("roi_" + std::to_string(e)))->setValidator(new QIntValidator(0, 100000));
	}
}

ConfigureView::~ConfigureView()
{

}


void ConfigureView::showFileChooserFor(int scenario){

	QFileDialog fileDialog(this);
	fileDialog.setFileMode(QFileDialog::FileMode::AnyFile);

	switch (scenario){
	case FileDialogSceneario::scenario::OPEN_CONFIGURATION:
		fileDialog.setNameFilter("YML files(*.yml)");
		fileDialog.setAcceptMode(QFileDialog::AcceptMode::AcceptOpen);
		break;
	case FileDialogSceneario::scenario::SAVE_CONFIGURATION:
		fileDialog.setNameFilter("YML files(*.yml)");
		fileDialog.setAcceptMode(QFileDialog::AcceptMode::AcceptSave);
		break;
	case FileDialogSceneario::scenario::OPEN_MODEL_FILE:
		fileDialog.setNameFilter("STL files(*.stl)");
		fileDialog.setAcceptMode(QFileDialog::AcceptMode::AcceptOpen);
		break;

	case FileDialogSceneario::scenario::SAVE_PCL:
		fileDialog.setNameFilter("PCD files(*.pcd)");
		fileDialog.setAcceptMode(QFileDialog::AcceptMode::AcceptSave);
		break;
	}

	QString fileName;
	if (!fileDialog.exec())
		return;

	fileName = fileDialog.selectedFiles()[0];

	switch (scenario){
	case FileDialogSceneario::scenario::OPEN_CONFIGURATION:
		ui.configuration_path->setText(fileName);
		itr::ApplicationConfiguration::readFromFile(fileName.toLocal8Bit().data(), _appConfig);
		loadConfiguration();
		break;
	case FileDialogSceneario::scenario::SAVE_CONFIGURATION:
		getFormConfiguration();
		itr::ApplicationConfiguration::writeToFile(fileName.toLocal8Bit().data(), _appConfig);
		break;
	case FileDialogSceneario::scenario::OPEN_MODEL_FILE:
		_appConfig.modelPath = qPrintable(fileName);
		ui.model_path->setText(QString::fromStdString(_appConfig.modelPath));
		break;

	case FileDialogSceneario::scenario::SAVE_PCL:
		// Save the pointcloud to a .PCD file
		osg::ref_ptr<osg::Vec3Array> pointsForPointcloud = _resultCloud;
		onProgressUpdate(0, "Saving point cloud");
		if (itr::utils::savePCDFile(pointsForPointcloud, fileName.toStdString())) {
			onProgressUpdate(0, "Pointcloud saved");
		}
		else onProgressUpdate(0, "Error saving point cloud");
		break;
	}
}


void ConfigureView::onProgressUpdate(double progress, QString progressDescription)
{
	ui.progressBar->setValue((int)progress);
	ui.info_label->setText(progressDescription);
}

void ConfigureView::onStateUpdate(int previous, int current){
	switch (current){
	case itr::ScanProcess::State::RUNNING:
		setEnabled(false);
		break;
	case itr::ScanProcess::State::ABORTED:
	case itr::ScanProcess::State::ENDED:

		_resultCloud = _runningScanProcess->getResultCloud();

		if (_resultCloud)
			ui.save_cloud->setEnabled(true);

		delete(_runningScanProcess);
		setEnabled(true);
		break;
	}
}


void ConfigureView::loadConfiguration(){

	ui.model_path->setText(QString::fromStdString(_appConfig.modelPath));

	itr::Scanner::Settings& scannerSettings = _appConfig.scannerSettings;

	for (int e = 0; e < 9; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("fundamental_" + std::to_string(e)))
			->setText(QString::number(scannerSettings.fundamentalMatrix.at<float>(e / 3, e % 3)));
	}

	for (int e = 0; e < 5; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("distortion_coefficients_" + std::to_string(e)))
			->setText(QString::number(scannerSettings.distortionCoefficients.at<float>(0, e)));
	}

	ui.focal_length->setText(QString::number(scannerSettings.focalLength));

	ui.sensor_width->setText(QString::number(scannerSettings.sensorSize.width));
	ui.sensor_height->setText(QString::number(scannerSettings.sensorSize.height));
	ui.camera_fps->setText(QString::number(scannerSettings.cameraFPS));

	ui.lasers_distance->setText(QString::number(scannerSettings.laserDistance));
	ui.lasers_aperture->setText(QString::number(scannerSettings.laserAperture));
	ui.lasers_angle->setText(QString::number(scannerSettings.laserAngle));

	ui.scan_mode->setCurrentIndex(scannerSettings.scanMode - 1);
	for (int e = 0; e < 3; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("starting_position_" + std::to_string(e)))->setText(QString::number(scannerSettings.startPosition[e]));
		this->findChild<QLineEdit *>(QString::fromStdString("direction_" + std::to_string(e)))->setText(QString::number(scannerSettings.direction[e]));
		this->findChild<QLineEdit *>(QString::fromStdString("looking_" + std::to_string(e)))->setText(QString::number(scannerSettings.looking[e]));
	}

	ui.brightness_threshold->setText(QString::number(scannerSettings.brightnessThreshold));
	ui.scansion_length->setText(QString::number(scannerSettings.scanLength));
	ui.speed->setText(QString::number(scannerSettings.speed));


	for (int e = 0; e < 4; ++e){
		this->findChild<QLineEdit *>(QString::fromStdString("roi_" + std::to_string(e)))->setText(QString::number(scannerSettings.rois[e]));
	}
}


void ConfigureView::getFormConfiguration(){

	_appConfig.modelPath = qPrintable(ui.model_path->text());

	itr::Scanner::Settings &scannerSettings = _appConfig.scannerSettings;
	for (int e = 0; e < 9; ++e){
		scannerSettings.fundamentalMatrix.at<float>(e / 3, e % 3) = this->findChild<QLineEdit*>(QString::fromStdString("fundamental_" + std::to_string(e)))->text().toFloat();
	}

	for (int e = 0; e < 5; ++e){
		scannerSettings.distortionCoefficients.at<float>(0, e) = this->findChild<QLineEdit*>(QString::fromStdString("distortion_coefficients_" + std::to_string(e)))->text().toFloat();
	}

	scannerSettings.focalLength = ui.focal_length->text().toFloat();

	scannerSettings.sensorSize.width = ui.sensor_width->text().toInt();
	scannerSettings.sensorSize.height = ui.sensor_height->text().toInt();
	scannerSettings.cameraFPS =  ui.camera_fps->text().toFloat();
	scannerSettings.laserDistance =	ui.lasers_distance->text().toFloat();
	scannerSettings.laserAperture = ui.lasers_aperture->text().toFloat();
	scannerSettings.laserAngle = ui.lasers_angle->text().toFloat();

	for (int e = 0; e < 3; ++e){
		scannerSettings.startPosition[e] = this->findChild<QLineEdit*>(QString::fromStdString("starting_position_" + std::to_string(e)))->text().toFloat();
		scannerSettings.direction[e] = this->findChild<QLineEdit*>(QString::fromStdString("direction_" + std::to_string(e)))->text().toFloat();
		scannerSettings.looking[e] = this->findChild<QLineEdit*>(QString::fromStdString("looking_" + std::to_string(e)))->text().toFloat();
	}
	scannerSettings.scanMode = static_cast<itr::Scanner::Settings::ScanMode>( ui.scan_mode->currentIndex() + 1);

	scannerSettings.brightnessThreshold = ui.brightness_threshold->text().toFloat();
	scannerSettings.scanLength = ui.scansion_length->text().toFloat();
	scannerSettings.speed = ui.speed->text().toFloat();


	for (int e = 0; e < 4; ++e){
		scannerSettings.rois[e] = this->findChild<QLineEdit*>(QString::fromStdString("roi_" + std::to_string(e)))->text().toFloat();
	}

}


void ConfigureView::workerUpdate(float progress){

}

void ConfigureView::showConfigurationErrorsDialog() {

	QMessageBox msgBox;	// Dialog box for eventual warnings

	std::string msgBoxText;
	for (int i = 0; i < _appConfig.getErrors().size(); ++i) {

		std::string error = _appConfig.getErrors().at(i);
		loginfo << error << std::endl;

		msgBoxText += error + "\n";

	}

	msgBox.setText(QString::fromStdString(msgBoxText));
	msgBox.exec();	// show each warning in a dialbox

}

void ConfigureView::start(){

	getFormConfiguration();
	loadConfiguration(); //repopulate form, that will show what was read, eg: how was read the value 9.9.9?
	if (!_appConfig.validate()){
		showConfigurationErrorsDialog();
		return;
	}
	
	lognotice << "Leaving configuration view with config:" << std::endl << _appConfig << std::endl;

	_runningScanProcess = new itr::ScanProcess(_appConfig);
	connect(_runningScanProcess, SIGNAL(progressUpdate(double, QString)), this, SLOT(onProgressUpdate(double, QString)));
	connect(_runningScanProcess, SIGNAL(stateUpdate(int, int)), this, SLOT(onStateUpdate(int, int)));
	_runningScanProcess->start();

	
}