#ifndef CONFIGUREVIEW_H
#define CONFIGUREVIEW_H

#include <QtWidgets/QMainWindow>
#include <QCloseEvent>
#include <QtCore\QSignalMapper>
#include <QtWidgets\QFileDialog>
#include <QtWidgets\QMessageBox>
#include "itr/ScanProcess.h"
#include "ui_configureview.h"
#include "itr/ApplicationConfiguration.h"


class FileDialogSceneario : public QObject
{
	Q_OBJECT

public:
	enum scenario{
		OPEN_CONFIGURATION,
		SAVE_CONFIGURATION,
		SAVE_PCL,
		OPEN_MODEL_FILE
	};

};

class ConfigureView : public QMainWindow
{
	Q_OBJECT
	Q_ENUMS(FileDialogScenario)

public:
	ConfigureView(QWidget *parent = 0);
	~ConfigureView();

	void showConfigurationErrorsDialog();

	public slots:
	void showFileChooserFor(int);
	void workerUpdate(float progress);
	void start();

	void onProgressUpdate(double progress, QString progressDescription);
	void onStateUpdate(int previous, int current);

private:
	Ui::ConfigureViewClass ui;

	void getFormConfiguration();
	void loadConfiguration();
	void notifyError();

	QString errorMsg;

	QProgressBar * progressBar;

	//int checkConfiguration(ApplicationConfiguration& config);

	void updateStatusBar(std::string message);
	void resetStatusBar(); // to be called by the constructor and at the viewer closure

	void updateProgressBar(float progress);
	void resetProgressBar(); // to be called by the constructor and at the viewer closure


	itr::ApplicationConfiguration _appConfig;

	itr::ScanProcess* _runningScanProcess = NULL;

	void closeEvent(QCloseEvent *evento){
		
		if (_runningScanProcess && _runningScanProcess->getState() == itr::ScanProcess::RUNNING){
			QMessageBox msgBox;
			msgBox.setText("Are you sure to exit?");
			msgBox.setInformativeText("Any change will be discarded");
			msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok );
			msgBox.setDefaultButton(QMessageBox::Cancel);
			int ret = msgBox.exec();

			switch (ret) {
			case QMessageBox::Cancel:
				evento->ignore();
				break;
			case QMessageBox::Ok:
				QMainWindow::closeEvent(evento);
				break;
			default:
				break;
			}
		}
	}


	osg::ref_ptr<osg::Vec3Array> _resultCloud;
};

#endif // CONFIGUREVIEW_H
