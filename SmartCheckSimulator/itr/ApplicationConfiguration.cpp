#include "ApplicationConfiguration.h"

namespace itr
{

	//serialization methods
	void ApplicationConfiguration::write(cv::FileStorage& fs) const
	{
		fs << "{";
		fs << "model_path" << modelPath;
		fs << "scanner" << scannerSettings;
		fs << "}"; //configuration
	}

	void ApplicationConfiguration::read(const cv::FileNode& node)  //Read serialization for this class
	{
		node["model_path"] >> this->modelPath;
		node["scanner"] >> scannerSettings;
	}

	int ApplicationConfiguration::readFromFile(std::string fileName, ApplicationConfiguration& configuration)
	{
		cv::FileStorage fs;

		fs = cv::FileStorage(fileName, cv::FileStorage::READ);
		if (!fs.isOpened())
		{
			fs.release();
			return 1;
		}

		fs["configuration"] >> configuration;
		fs.release();

		return 0;
	}

	void ApplicationConfiguration::writeToFile(std::string fileName, ApplicationConfiguration &configuration)
	{
		cv::FileStorage fs;

		fs = cv::FileStorage(fileName, cv::FileStorage::WRITE);

		fs << "configuration" << configuration;
		fs.release();
		return;
	}

	int ApplicationConfiguration::readFromINIFile(std::string fileName, ApplicationConfiguration &configuration){
		
		OSG_NOTICE << "Reading configuration file..." << std::flush;
		INIReader reader(fileName);
		
		if (reader.ParseError() < 0) {
			OSG_NOTICE << "Error loading ini file: " << fileName << std::endl;
			return -1;
		}

		itr::Scanner::Settings &settings = configuration.scannerSettings;
		settings.sensorSize.width = reader.GetInteger("Camera", "image_width", -1);
		settings.sensorSize.height = reader.GetInteger("Camera", "image_height", -1);
		settings.focalLength = reader.GetReal("Camera", "focal_length", -1.0);

		double pixelSize = reader.GetReal("Camera", "pixel_size", -1.0);
		settings.fundamentalMatrix.at<float>(0, 0) = reader.GetReal("Camera Calibration", "Fx", -1.0);
		settings.fundamentalMatrix.at<float>(1, 1) = reader.GetReal("Camera Calibration", "Fy", -1.0);
		settings.fundamentalMatrix.at<float>(0, 2) = reader.GetReal("Camera Calibration", "Cx", -1.0);
		settings.fundamentalMatrix.at<float>(1, 2) = reader.GetReal("Camera Calibration", "Cy", -1.0);
		settings.fundamentalMatrix.at<float>(2, 2) = 1.0;

		std::string dCoeffLabel = "D";

		for (int i = 0; i < settings.distortionCoefficients.cols; ++i){
			settings.distortionCoefficients.at<float>(0, i) 
				= reader.GetReal("Camera Calibration", dCoeffLabel + std::to_string(i), INFINITE);
		}

		settings.laserAngle = reader.GetReal("Laser", "Inclination", -1.0);
		settings.laserAperture = reader.GetReal("Laser", "FanAngle", -1.0);
		settings.laserDistance = reader.GetReal("Laser", "Baseline", -1.0);

		settings.looking = osg::Vec3(0.0, 0.0, -1.0);
		settings.direction = osg::Vec3(0.0, 1.0, 0.0);
		
		settings.startPosition.x() = reader.GetReal("Execution", "Camera_Start_Pos_X", INFINITE);
		settings.startPosition.y() = reader.GetReal("Execution", "Camera_Start_Pos_Y", INFINITE);
		settings.startPosition.z() = reader.GetReal("Execution", "Camera_Start_Pos_Z", INFINITE);

		settings.scanLength = reader.GetReal("Execution", "Scan_length", -1.0);
		settings.speed = reader.GetReal("Execution", "Speed", -1.0);
		settings.cameraFPS = reader.GetReal("Execution", "FPS", -1.0);

		settings.rois[0] = reader.GetReal("Execution", "ROI1_Y", -1.0);
		settings.rois[1] = reader.GetReal("Execution", "ROI1_H", -1.0);
		settings.rois[2] = reader.GetReal("Execution", "ROI2_Y", -1.0);
		settings.rois[3] = reader.GetReal("Execution", "ROI2_H", -1.0);

		settings.brightnessThreshold = 0.7;
		settings.scanMode = Scanner::Settings::ScanMode::MODEL_UNDER_CROSS;

		OSG_NOTICE << "DONE" << std::endl;

		return 0;
	}

	std::ostream &operator<<(std::ostream &os, ApplicationConfiguration &config)
	{
		os << "model: " << config.modelPath << std::endl;
		os << "scanner: " << std::endl;
		os << config.scannerSettings;

		return os;
	}

	bool ApplicationConfiguration::validate()
	{
		_errors.clear();

		scannerSettings.validate();
		_errors = scannerSettings.getErrors();
		
		return _errors.empty();
	}

}