#pragma once

#include <osg/Vec3>
#include "itr/Serializable.h"
#include "itr/Scanner.h"
#include "../ini/INIReader.h"

namespace itr
{

	/*
	class ApplicationConfiguration

	This class is used to read or write files with our specific configuration files
	*/
	class ApplicationConfiguration : 
		public Serializable
	{

	public:

		static int readFromFile(std::string fileName, ApplicationConfiguration &configuration);
		static void writeToFile(std::string fileName, ApplicationConfiguration &configuration);

		static int readFromINIFile(std::string fileName, ApplicationConfiguration &configuration);

		/*
		modelPath

		file path of the 3D model to scan
		*/
		std::string modelPath;


		bool showResult = true;

		/*
		scannerSettings

		scan configuration to apply
		*/
		Scanner::Settings scannerSettings;


		/*
		validate()

		check the rules for this configuration, if all it's ok return true
		*/
		bool validate();


		/*
		getErrors()

		return errors associated with this configuration after validate()
		*/
		inline const std::vector<std::string> getErrors()
		{
			return _errors;
		}

		/*
		* opencv serializable methods implementation
		*/
		void write(cv::FileStorage& fs) const;
		void read(const cv::FileNode& node);

		/*
		for ostream printing
		*/
		friend std::ostream& operator<< (std::ostream &out, const ApplicationConfiguration &point);


	private:
		//errors in configuration
		std::vector<std::string> _errors;
	};

	std::ostream &operator<<(std::ostream &os, ApplicationConfiguration &config);
}