#include "FrameProcessor.h"


namespace itr
{
	
	FrameProcessor::FrameProcessor(itr::Scanner* scanner)
	{
		_scanner = scanner;		
		setDataVariance(Drawable::DataVariance::DYNAMIC);

		int N_ROIS = 2;
		int N_ROWS_PER_ROI = 2; // one row stores y position of the brightest pixels for each sensor column, the other row stores the brightness of that pixel
		int nColumns = scanner->getImageSize().width;
		osg::Vec4i rois = scanner->getRois();

		// bind image to the compure shader
		scanner->getTexture()->setInternalFormat(GL_RGBA8);
		scanner->getTexture()->setSourceFormat(GL_RGBA); // without this it doesn't work 
		//scanner->getTexture()->setSourceType(GL_UNSIGNED_BYTE); 
		scanner->getTexture()->bindToImageUnit(0,osg::Texture::READ_ONLY);  // so that we can use a 'image2D' in the compute shader
		
		getOrCreateStateSet()->setTextureAttributeAndModes(0, scanner->getTexture());
		getOrCreateStateSet()->getOrCreateUniform("targetImage", osg::Uniform::Type::IMAGE_2D)->set(0);

		getStateSet()->getOrCreateUniform("rois", osg::Uniform::INT_VEC4)->set(rois[0], rois[1], rois[2], rois[3]);
		getStateSet()->getOrCreateUniform("imageSize", osg::Uniform::INT_VEC2)->set(scanner->getImageSize().width, scanner->getImageSize().height);


		// Load compute shader
		osg::ref_ptr<osg::Shader> shader = new osg::Shader(osg::Shader::COMPUTE);
		
		/*
		//load shader from file
		shader->loadShaderSourceFromFile("shaders/lightpixels.cs");
		*/
		shader->setShaderSource(itr::shaders::PROCESSOR_COMPUTE_SHADER);

		computeProg = new osg::Program;
		computeProg->setComputeGroups(nColumns / 64, 1, 1);

		computeProg->addShader(shader);


		// Create a buffer for storing world reconstructed points
		int FLOAT_PER_VEC3 = 3;
		initialBuffer = new osg::FloatArray();
		initialBuffer->resize(1 + scanner->getImageSize().width * FLOAT_PER_VEC3 *N_ROIS* N_WAIT_FRAMES);

		memset((void*)initialBuffer->getDataPointer(), 0, initialBuffer->size()*sizeof(GL_FLOAT));
		pointsBufferObject = new osg::ShaderStorageBufferObject;
		pointsBufferObject->setUsage(GL_DYNAMIC_READ);
		initialBuffer->setBufferObject(pointsBufferObject);


		// Create pointsBufferBinding
		osg::ref_ptr<osg::ShaderStorageBufferBinding> pointsBufferBinding =
			new osg::ShaderStorageBufferBinding(0, pointsBufferObject, 0, initialBuffer->size()*sizeof(GL_FLOAT));

		
		getOrCreateStateSet()->setAttributeAndModes(pointsBufferBinding, osg::StateAttribute::ON);
		getOrCreateStateSet()->setAttributeAndModes(computeProg, osg::StateAttribute::ON);


		_worldToPixelsRatios = osg::Vec2 (
			scanner->getFundamentalMatrix().at<float>(0, 0) / _scanner->getFocalLength(),
			scanner->getFundamentalMatrix().at<float>(1, 1) / _scanner->getFocalLength()
			);


		// Compute the laser geometries into the scanner coordinates space
		for (int i = 0; i < scanner->N_LASERS; ++i)
		{
			osg::ref_ptr<Laser> laser = this->_scanner->getLasers()[0];
			osg::Vec3 laserLimitScannerCoordinates[2];
			laserLimitScannerCoordinates[0] = osg::Vec3(laser->getBeamLimits()[0].x(), laser->getBeamLimits()[0].y(), -laser->getBeamLimits()[0].z());
			laserLimitScannerCoordinates[1] = osg::Vec3(laser->getBeamLimits()[1].x(), laser->getBeamLimits()[1].y(), -laser->getBeamLimits()[1].z());
			laserLimitScannerCoordinates[0].normalize();
			laserLimitScannerCoordinates[1].normalize();
			this->laserNormalScannerCoords[i] = laserLimitScannerCoordinates[0] ^ laserLimitScannerCoordinates[1];
			this->laserNormalScannerCoords[i].normalize();
		}

		// lightpixelsBin contains data readed back from GPU
		
		this->_worldPoints = new osg::Vec3Array;
		_isLastFrame = false;
	}


	void FrameProcessor::update()
	{

		osg::StateSet* ss = getOrCreateStateSet();

		osg::Uniform *laserOrigins = ss->getOrCreateUniform("laserOrigins", osg::Uniform::FLOAT_VEC3, 2);
		laserOrigins->setElement(0, _scanner->getLasers()[0]->getOrigin());
		laserOrigins->setElement(1, _scanner->getLasers()[1]->getOrigin());

		osg::Uniform *laserNormals = ss->getOrCreateUniform("laserNormals", osg::Uniform::FLOAT_VEC3, 2);
		laserNormals->setElement(0, _scanner->getLasers()[0]->getPlaneNormal());
		laserNormals->setElement(1, _scanner->getLasers()[1]->getPlaneNormal());

		osg::Vec2f sensorSize(_scanner->getImageSize().width, _scanner->getImageSize().height);

		osg::Vec3 sensorCenter((sensorSize.x() - 1.0) / 2.0, (sensorSize.y() - 1.0) / 2.0, 0.0);

		osg::Matrix worldToPixelCoord =
			_scanner->getViewMatrix()*
			osg::Matrix::scale(_worldToPixelsRatios[0], -_worldToPixelsRatios[1], _worldToPixelsRatios[0])*
			osg::Matrix::translate(sensorCenter.x(), sensorCenter.y(), 0.0);

		osg::Matrix pixelToWorldCoord = osg::Matrix::inverse(worldToPixelCoord);

		ss->getOrCreateUniform("worldToPixelCoord", osg::Uniform::FLOAT_MAT4)->set(worldToPixelCoord);
		ss->getOrCreateUniform("pixelToWorldCoord", osg::Uniform::FLOAT_MAT4)->set(pixelToWorldCoord);
		ss->getOrCreateUniform("focalLengthPC", osg::Uniform::FLOAT)->set(_scanner->getFocalLength()*_worldToPixelsRatios[0]);
		ss->getOrCreateUniform("brightnessThreshold", osg::Uniform::FLOAT)->set(_scanner->getBrightnessThreshold());
		ss->getOrCreateUniform("scanMode", osg::Uniform::UNSIGNED_INT)->set(_scanner->getScanMode());
	}


#define MAPPING_TOO_SLOW 0

	void FrameProcessor::FinalDrawCallBack::operator()(osg::RenderInfo& renderInfo) const
	{

		++_frameProcessor->frameCounter;


		if ((_frameProcessor->frameCounter % _frameProcessor->N_WAIT_FRAMES ) == 0 && !_frameProcessor->_isLastFrame)
		{
			// we wait some other frames
			return;
		}
		_frameProcessor->frameCounter = 0;

		osg::GLBufferObject * bo;
		bo = _frameProcessor->pointsBufferObject->getGLBufferObject(renderInfo.getContextID());
		
	#if MAPPING_TOO_SLOW

		int nPointsIHave;
		bo->_extensions->glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GL_INT), &nPointsIHave);

		int pointsSpaceSize = nPointsIHave*sizeof(GL_FLOAT)*3;
		float * values = _frameProcessor->computedPointsBin;;
		bo->_extensions->glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, sizeof(GL_INT), pointsSpaceSize, values);
		float *p = values;
		for (int i = 0; i < nPointsIHave; ++i)
		{
			p = p + 3;
			_frameProcessor->getWorldPoints()->push_back(osg::Vec3(p[0], p[1], p[2]));
		}

		nPointsIHave = 0;
		bo->_extensions->glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GL_INT), &nPointsIHave);

	#else

		// With mapping try

		int nPointsIHave;
		int * nPointsPtr;
		osg::GLExtensions * gl = bo->_extensions;
		
		gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, bo->getGLObjectID());
		

		// Read how many points have been computed
		nPointsPtr = (GLint*)gl->glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GL_INT), GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);	
		nPointsIHave = *nPointsPtr;
		*nPointsPtr = 0;
		gl->glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		
		
		// Read the recontructed points
		if (nPointsIHave > 0)
		{
			int pointsSpaceSize = (nPointsIHave)*sizeof(GL_FLOAT) * 3;
			float * values;
			values = (GLfloat*)gl->glMapBufferRange(GL_SHADER_STORAGE_BUFFER, sizeof(GL_INT), pointsSpaceSize, GL_MAP_READ_BIT);

			/*lognotice << glGetError() << std::endl;
			lognotice << GL_INVALID_ENUM << std::endl;
			lognotice << GL_INVALID_VALUE << std::endl;
			lognotice << GL_INVALID_OPERATION << std::endl;
			lognotice << GL_NO_ERROR << std::endl;*/

			float *p = values;
			for (int i = 0; i < nPointsIHave; ++i)
			{
				p = p + 3;
				_frameProcessor->getWorldPoints()->push_back(osg::Vec3(p[0], p[1], p[2]));
			}

			gl->glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		}

		gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	#endif

	}

}