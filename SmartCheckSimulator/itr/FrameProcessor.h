#pragma once

#include <osg/Drawable>
#include <osg/BufferIndexBinding>
#include <osg/Texture2D>
#include <osg/LineSegment>
#include <osg/State>
#include "itr/Utils.h"
#include "itr/Scanner.h"

namespace itr
{

	class Scanner;

	/*
	class FrameProcessor

	This class represent a software that process each frame produced
	by the associated scanner
	*/
	class FrameProcessor : 
		public osg::Drawable
	{
		
	public:
			/*
			After frame processor draw callback: here we can read updated values
			of frame pixels
			*/
		class FinalDrawCallBack : 
			public osg::Camera::DrawCallback
		{
		
		public:

			FinalDrawCallBack(osg::ref_ptr<FrameProcessor> frameProcessor)
			{
				_frameProcessor = frameProcessor;
			}
			virtual void operator()(osg::RenderInfo& renderInfo) const;

		private:

			osg::ref_ptr<FrameProcessor> _frameProcessor;
		};
	

		FrameProcessor(itr::Scanner* scanner);
		
		inline Scanner* getScanner()
		{
			return _scanner;
		}
		
		/*
			getWorldPoints()

			return the reconstructed points
		*/
		inline osg::ref_ptr<osg::Vec3Array> getWorldPoints() const
		{
			return _worldPoints;
		}

		/*
		setIsLastFrame()

		inform the frame processor if this is the last frame so it must
		download all new data
		*/
		inline void setIsLastFrame(bool isLastFrame)
		{
			_isLastFrame = isLastFrame;
		}
		
		/*
		update()

		update computate shader uniforms 
		*/
		void update();


private:

		osg::ref_ptr<osg::ShaderStorageBufferObject> pointsBufferObject;
		osg::ref_ptr<osg::FloatArray> initialBuffer;

		osg::ref_ptr<osg::Program> computeProg;
	
		osg::ref_ptr<itr::Scanner> _scanner;

		osg::ref_ptr<osg::Vec3Array> _worldPoints;

		osg::Vec2 _worldToPixelsRatios;
		osg::Vec3 laserNormalScannerCoords[2];
		
		bool _isLastFrame;

		int frameCounter = 0;

		/*
		N_WAIT_FRAMES

		frames to wait before downloading data from gpu
		*/
		const int N_WAIT_FRAMES = 10;
	};

}