#include "Laser.h"

using namespace std;

namespace itr
{

	Laser::Laser(Scanner * scanner, osg::Vec3 origin, osg::Vec3 * beamLimits, ScannerSide side, osg::Vec2i textureSize) :
		RTTDepthCamera(textureSize)
	{
		lognotice << "Creating " << (side == ScannerSide::SCANNER_FRONT_SIDE ? "front" : "rear") << " laser" << endl;

		_scanner = scanner;
		_side = side;

		setBeamLimits(beamLimits);
		setOrigin(origin);
		
		initialize();
	}

	Laser::~Laser()
	{
	}

	void Laser::initialize()
	{
		// Enable backfaces culling
		osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace(osg::CullFace::Mode::BACK);
		getOrCreateStateSet()->setAttributeAndModes(cullFace, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

		setRenderOrder(RenderOrder::PRE_RENDER);
		laserIndex = _side == ScannerSide::SCANNER_FRONT_SIDE ? 0 : 1;
		laserUniformName = "lasers[" + std::to_string(laserIndex) + "].";

		setCullingMode(osg::CullSettings::VIEW_FRUSTUM_CULLING);
		needRefresh = true;
	}

	void Laser::update(float deltaMillis)
	{
		// Update scanner position relative to the scanner
		this->setOrigin(getScanner()->getDirection()*getScanner()->getLaserDistance()*_side
			+ getScanner()->getPosition()
			- _scanner->getLooking()*_scanner->getFocalLength());

		if (needRefresh)
			refresh();

		osg::ref_ptr<osg::StateSet> stateSet = getScanner()->getOrCreateStateSet();
		osg::Matrixd mvptMatrix;
		osg::Vec3 lookAt = getOrigin() + (getBeamLimits()[0] + getBeamLimits()[1]) * 100;

		osg::Vec3 up = getPlaneNormal();
		up.normalize();
		setViewMatrixAsLookAt(getOrigin(), lookAt, up); //!!!
		

		// We need to compute near and far planes
		if (Node * model = getChild(0))
		{
			osg::BoundingSphere sceneBound = model->getBound();

			if (sceneBound.valid())
			{
				float nearPlane = (getOrigin() - sceneBound.center()).length() - sceneBound.radius();

				if (nearPlane < 10.0)
				{
					nearPlane = 10.0;
				}

				float farPlane = nearPlane + sceneBound.radius()*2.0;

				float shadowCamFovY = 2.0* osg::RadiansToDegrees(std::atan(_lineSize / 2.0 / nearPlane));
				float ratio = _laserFov / shadowCamFovY;

				// this may be changed to something more specific for our application //!!!
				setProjectionMatrixAsPerspective(shadowCamFovY, ratio, nearPlane, farPlane);
			}
		}	

		// To depth map texture coordinates matrix
		mvptMatrix =
			getViewMatrix() *
			getProjectionMatrix() *
			osg::Matrix::translate(1.0, 1.0, 1.0) * // same as device coordinates matrix translated by (1,1,1) ...
			osg::Matrix::scale(0.5f, 0.5f, 0.5f); // ... and scaled by (0.5,0.5,0.5)

		stateSet->getOrCreateUniform(laserUniformName + "origin", osg::Uniform::Type::FLOAT_VEC3)->set(getOrigin());
		stateSet->getOrCreateUniform(laserUniformName + "depthMVP", osg::Uniform::Type::FLOAT_MAT4)->set(mvptMatrix);
	}

	void Laser::refresh()
	{
		_laserFov = osg::RadiansToDegrees(acos(getBeamLimits()[0] * this->getBeamLimits()[1]));

		osg::ref_ptr<osg::StateSet> stateSet = getScanner()->getOrCreateStateSet();

		stateSet->getOrCreateUniform(laserUniformName + "origin", osg::Uniform::Type::FLOAT_VEC3)->set(getOrigin());

		osg::Vec3 beamBoundNormals[2] =
		{
			getPlaneNormal() ^ getBeamLimits()[0],
			-getPlaneNormal() ^ getBeamLimits()[1],
		};

		beamBoundNormals[0].normalize();
		beamBoundNormals[1].normalize();

		stateSet->getOrCreateUniform(laserUniformName + "bound1", osg::Uniform::Type::FLOAT_VEC3)->set(beamBoundNormals[0]);
		stateSet->getOrCreateUniform(laserUniformName + "bound2", osg::Uniform::Type::FLOAT_VEC3)->set(beamBoundNormals[1]);
		stateSet->getOrCreateUniform(laserUniformName + "beamPlaneNormal", osg::Uniform::Type::FLOAT_VEC3)->set(getPlaneNormal());

		stateSet->getOrCreateUniform(laserUniformName + "color", osg::Uniform::Type::FLOAT_VEC3)->set(osg::Vec3(getColor().x(), getColor().y(), getColor().z()));
		stateSet->getOrCreateUniform(laserUniformName + "lineSize", osg::Uniform::Type::FLOAT)->set(_lineSize);
		stateSet->getOrCreateUniform(laserUniformName + "depthMapTexture", osg::Uniform::Type::INT)->set(laserIndex);

		stateSet->getOrCreateUniform("ShadowMap", osg::Uniform::SAMPLER_2D, 2)->setElement(laserIndex, laserIndex);
		stateSet->setTextureAttributeAndModes(laserIndex, getTexture(), osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

		needRefresh = false;
	}


	void Laser::computeLaserBeamLimits(double aperture, double angle, osg::Vec3 rotationAxis, osg::Vec3 beamNormal, osg::Vec3 direction, osg::Vec3 * beams)
	{
		double rotationAngle = osg::DegreesToRadians(angle);
		beamNormal.normalize();
		rotationAxis.normalize();

		double beamLimitAngleFromDirection = osg::DegreesToRadians(aperture / 2.0f);

		beams[0] = osg::Quat(-beamLimitAngleFromDirection, beamNormal) * direction;
		beams[1] = osg::Quat(beamLimitAngleFromDirection, beamNormal) * direction;

		osg::Quat rotation = osg::Quat(-rotationAngle, rotationAxis);
		beams[0] = rotation * beams[0];
		beams[1] = rotation * beams[1];
	}

}