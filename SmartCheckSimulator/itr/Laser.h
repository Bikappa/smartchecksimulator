#pragma once

#include <osg/Vec3>
#include <osg/Vec4>
#include <osg/Quat>
#include "itr/utils.h"
#include "itr/Scanner.h"
#include "itr/RTTDepthCamera.h"

namespace itr
{

	class Scanner;

	/*
	class Laser

	This class represents a laser projector. 
	It renders the depth buffer into a texture
	*/
	class Laser :
		public RTTDepthCamera
	{
	
	public:

		typedef enum scannerSide
		{
			SCANNER_FRONT_SIDE = 1,
			SCANNER_REAR_SIDE = -1
		} ScannerSide;


		Laser(Scanner *scanner, osg::Vec3 origin, osg::Vec3 * beamLimits, ScannerSide side, osg::Vec2i textureSize);
		~Laser();

		void Laser::initialize();

		/*
		update()

		update laser position and uniforms
		*/
		void update(float deltaMillis);

		/*
		refresh()

		hard update laser parameters
		*/
		void refresh();


		inline ScannerSide getScannerSide()
		{
			return _side;
		}

		inline osg::Vec3 getOrigin()
		{
			return _origin;
		}

		inline osg::Vec4 getColor()
		{
			return _color;
		}

		inline void setColor(osg::Vec4 color)
		{
			_color = color;
			needRefresh = true;
		}

		inline void setOrigin(osg::Vec3 origin)
		{
			_origin = origin;

		}

		inline const osg::Vec3 * getBeamLimits()
		{
			return _beamLimits;
		}

		inline void setBeamLimits(osg::Vec3 * limits)
		{
			for (int i = 0; i < 2; ++i){
				_beamLimits[i] = limits[i];
				_beamLimits[i].normalize();
			}

			needRefresh = true;
		}

		inline Scanner* getScanner()
		{
			return _scanner;
		}

		inline osg::Vec3 getPlaneNormal()
		{
			osg::Vec3 crossProduct = _beamLimits[0] ^ _beamLimits[1];
			crossProduct.normalize();
			return crossProduct;
		}

		/*
		computeLaserBeamLimits()

		compute laser limits vector given the laser parameters
		*/
		static void Laser::computeLaserBeamLimits(
			const double aperture,
			const double angle,
			const osg::Vec3 rotationAxis,
			const osg::Vec3 beamNormal,
			const osg::Vec3 direction,
			osg::Vec3 * beams);

	private:
		
		Scanner* _scanner;
		ScannerSide _side;
		osg::Vec3 _origin;

		/*
		Laser beam limits vector
		Note that this 2 vectors jointly with the origin point fully
		describe the laser ray
		*/
		osg::Vec3 _beamLimits[2];

		// laser line size
		float _lineSize = 3.0;

		float _depthTextureWidth = 1000.0;
		float _depthTextureHeight = 80.0;

		// laser color, default is red
		osg::Vec4 _color = osg::Vec4(1.0, 0.0, 0.0, 1.0);

		int laserIndex;
		std::string laserUniformName;

		// laser field of view
		float _laserFov;

		bool needRefresh;
	};

}

