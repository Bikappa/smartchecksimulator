#include "RTTCamera.h"

namespace itr
{

	RTTCamera::RTTCamera(unsigned int textureWidth, unsigned int textureHeight, GLenum textureInternalFormat,
		Camera::BufferComponent toTextureBuffer)
	{

		_texture = new osg::Texture2D;
		_textureInternalFormat = textureInternalFormat;
		_toTextureBuffer = toTextureBuffer;
		_texture->setInternalFormat(_textureInternalFormat);
		_texture->setTextureSize(textureWidth, textureHeight);

		_texture->setWrap(osg::Texture2D::WRAP_S, osg::Texture2D::CLAMP_TO_BORDER);
		_texture->setWrap(osg::Texture2D::WRAP_T, osg::Texture2D::CLAMP_TO_BORDER);

		_texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
		_texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
		setViewport(0, 0, _texture->getTextureWidth(), _texture->getTextureHeight());

		setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);

		setReferenceFrame(Transform::ABSOLUTE_RF);
		attach(_toTextureBuffer, _texture);
		setRenderTargetImplementation(FRAME_BUFFER_OBJECT);

		setDrawBuffer(GL_NONE);
		setReadBuffer(GL_NONE);
	}

	RTTCamera::~RTTCamera()
	{
	}

}
