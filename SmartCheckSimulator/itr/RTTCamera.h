#pragma once

#include <osg/Camera>
#include <osg/Texture2D>
#include "itr/utils.h"

namespace itr
{

	/*
	class RTTCamera

	generic class for camera that renders a specific buffer to a texture
	*/
	class RTTCamera :
		public osg::Camera
	{

	public:

		RTTCamera(unsigned int textureWidth, unsigned int textureHeight, GLenum textureInternalFormat = GL_RGBA8,
			Camera::BufferComponent toTextureBuffer = Camera::BufferComponent::COLOR_BUFFER);
		~RTTCamera();

		inline osg::ref_ptr<osg::Texture2D> getTexture(){
			return _texture;
		}

	protected:

		Camera::BufferComponent _toTextureBuffer;
		GLenum _textureInternalFormat;
		osg::ref_ptr<osg::Texture2D> _texture;
	};

}