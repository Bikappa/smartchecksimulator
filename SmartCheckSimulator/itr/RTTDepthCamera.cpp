#include "itr/RTTDepthCamera.h"

namespace itr
{

	RTTDepthCamera::RTTDepthCamera(osg::Vec2i textureSize) :
		RTTCamera(textureSize.x(), textureSize.y(), GL_DEPTH_COMPONENT, DEPTH_BUFFER)
	{
		setReferenceFrame(Transform::ABSOLUTE_RF);
		setComputeNearFarMode(Camera::DO_NOT_COMPUTE_NEAR_FAR);

		//points at infinity are white
		setClearColor(osg::Vec4(1.0, 1.0, 1.0, 1.0));

		//we need to compute, so clear, only depth buffer
		setClearMask(GL_DEPTH_BUFFER_BIT);
		setViewport(0, 0, this->_texture->getTextureWidth(), this->_texture->getTextureHeight());
				
		setDrawBuffer(GL_NONE);
		setReadBuffer(GL_NONE);
	}

	RTTDepthCamera::~RTTDepthCamera()
	{
	}

}