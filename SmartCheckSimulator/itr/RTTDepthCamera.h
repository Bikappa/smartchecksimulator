#pragma once

#include "itr/RTTCamera.h"

namespace itr
{

	/*
	class RTTDepthCamera

	Represents a camera that renders its depth buffer into a shadow map texture
	*/
	class RTTDepthCamera :
		public RTTCamera
	{

	public:

		RTTDepthCamera(osg::Vec2i textureSize);
		~RTTDepthCamera();
	};

}