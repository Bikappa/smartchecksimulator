#include "RealCamera.h"

namespace itr
{

	RealCamera::RealCamera(cv::Mat fundamentalMatrix, cv::Size imageSize, cv::Mat distortionCoefficients) :
		RTTCamera(imageSize.width,imageSize.height)
	{
		_fundamentalMatrix = fundamentalMatrix;
		_imageSize = imageSize;
		_distortionCoefficients = distortionCoefficients;

		initialize();
	}

	RealCamera::~RealCamera()
	{
	}

	void RealCamera::initialize()
	{
		setClearColor(osg::Vec4(0,1,0,1));
		setRenderOrder(Camera::RenderOrder::NESTED_RENDER);
		setCullingMode(CullSettings::VIEW_FRUSTUM_CULLING);

		setProjectionMatrix(RealCamera::fundamentalToProjectionMatrix(_fundamentalMatrix, _imageSize));
	}


	osg::Matrix RealCamera::fundamentalToProjectionMatrix(cv::Mat fundamentalMatrix, cv::Size imageSize)
	{
		double f_x, f_y, x_o, y_o, fovy, ratio;
		double near_, far_;
		f_x = fundamentalMatrix.at<float>(0, 0);
		f_y = fundamentalMatrix.at<float>(1, 1);

		//x_o = fundamentalMatrix.at<float>(0, 2);
		//y_o = fundamentalMatrix.at<float>(1, 2);
		
		//no distortion
		x_o = imageSize.width / 2.0;
		y_o = imageSize.height / 2.0;

		float xDecentralization = x_o - imageSize.width / 2.0;
		int xside = xDecentralization > 0.0 ? 1 : -1;

		float yDecentralization = y_o - imageSize.height / 2.0;
		int yside = yDecentralization > 0.0 ? 1 : -1;

		float maxCenterDistX = imageSize.width / 2.0 + abs(xDecentralization);
		float maxCenterDistY = imageSize.height / 2.0 + abs(yDecentralization);

		
		fovy = osg::RadiansToDegrees(2.0 * atan(maxCenterDistY / f_y));
		ratio = maxCenterDistX / maxCenterDistY * f_x / f_y;

		//near and far planes will be automatically computed each frame
		near_ = 10;
		far_ = 20;

		osg::Matrix projection;
		projection.makePerspective(fovy, ratio, near_, far_);


		// For distortion simulation
		double sx = maxCenterDistX * 2 / imageSize.width;
		double sy = maxCenterDistY * 2 / imageSize.height;

		osg::Matrix ndc =
			projection *
			osg::Matrix::scale(sx, sy, 1.0) *
			osg::Matrix::translate(xside*(sx - 1.0), yside*(sy - 1.0), 0.0);

		float A = near_ + far_;
		float B = near_ * far_;

		osg::Matrix fundamental = osg::Matrix (
			f_x/2, 0, 0, 0,
			0, f_y/2, 0, 0,
			-x_o, -y_o, A, -1.0,
			0.0, 0.0, B, 0
			);

		ndc = fundamental * osg::Matrix::ortho(0, imageSize.width, 0, imageSize.height, near_, far_);

		return projection;
	}

}