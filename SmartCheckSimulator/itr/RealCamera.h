#pragma once

#include <osg/Camera>
#include <opencv2/core/core.hpp>
#include "itr/RTTCamera.h"

namespace itr
{

	/*
	class RealCamera

	This class represents the camera used to implmenent the camera of the scanner
	*/
	class RealCamera :
		public RTTCamera
	{

	public:

		RealCamera(cv::Mat fundamentalMatrix, cv::Size imageSize, cv::Mat distortionCoefficients = cv::Mat());
		RealCamera::~RealCamera();

		static osg::Matrix fundamentalToProjectionMatrix(cv::Mat fundamentalMatrix, cv::Size imageSize);

		cv::Mat getFundamentalMatrix()
		{
			return this->_fundamentalMatrix;
		}

		cv::Mat getDistortionCoefficients()
		{
			return this->_distortionCoefficients;
		}

		cv::Size getImageSize()
		{
			return this->_imageSize;
		}

	private:

		void initialize();
		cv::Mat _fundamentalMatrix;
		cv::Mat _distortionCoefficients;
		cv::Size _imageSize;
	};

}