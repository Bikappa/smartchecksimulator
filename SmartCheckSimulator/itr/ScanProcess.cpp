#include "itr/ScanProcess.h"

uint timeA, timeB;

namespace itr
{

	/*
	class SwitchResultShow

	This class implements the method to be triggered by the event happening
	*/
	class SwitchResultShow :
		public itr::utils::KeyPressedCallback
	{

	public:

		SwitchResultShow(int& mask) : _mask(mask){}

		virtual void action()
		{
			lognotice << "Switch show result" << std::endl;
			_mask++;
			if (_mask > ScanProcess::ResultShowMask::SHOW_MESH_AND_POINTS)
			{
				_mask = ScanProcess::ResultShowMask::SHOW_POINTS;
			}
		}

	private:
		int& _mask;
	};


	ScanProcess::ScanProcess(itr::ApplicationConfiguration  appConfig)
	{
		
		_appConfig = appConfig;

		lognotice << "Scan process constructor" << std::endl;
		lognotice << _appConfig << std::endl;
		changeState(READY);

	}

	void ScanProcess::run()
	{
		setPriority(QThread::TimeCriticalPriority);

		if (_state != READY){
			return;
		}

		changeState(RUNNING);

		beforeLoop();

		if (_state == ABORTED)
			return;

		loop();

		if (_state == ABORTED)
			return;

		afterLoop();

		changeState(ENDED);
	}

	void ScanProcess::changeProgress(double progress, std::string progressDescription)
	{
		_progress = progress;
		_progressDescription = QString::fromStdString(progressDescription);

		emit progressUpdate(_progress, _progressDescription);
	}

	void ScanProcess::changeState(State newState)
	{
		State previous = _state;
		_state = newState;

		emit stateUpdate(previous, newState);
	}

	void ScanProcess::beforeLoop()
	{
		std::string infoMessage = "Loading model: " + _appConfig.modelPath;
		changeProgress(0.0, infoMessage);

		timeA = QDateTime::currentMSecsSinceEpoch();
		_model = osgDB::readNodeFile(_appConfig.modelPath);
		timeB = QDateTime::currentMSecsSinceEpoch();


		if (_model == NULL)
		{
			changeProgress(0.0, "Fatal error loading model: " + _appConfig.modelPath);
			osg::notify(osg::FATAL) << infoMessage << std::endl;
			changeState(ABORTED);
			return;
		}

		OSG_NOTICE << "load model time (ms): " << timeB - timeA << std::endl;

		timeA = QDateTime::currentMSecsSinceEpoch();
		changeProgress(0.0, "Done");

		utils::setUseVBO(_model);
		_model->setDataVariance(osg::Object::DataVariance::STATIC);

		_scanner = new Scanner(_appConfig.scannerSettings);
		_scanner->setRenderOrder(osg::Camera::POST_RENDER, 0);;
		_scanner->addChild(_model);
		_scanner->update(0);

		osg::ref_ptr<osg::Geometry> quad = osg::createTexturedQuadGeometry(osg::Vec3(0, 0, 0), osg::Vec3(windowSize.x(), 0, 0), osg::Vec3(0, windowSize.y(), 0));
		osg::ref_ptr<osg::Geode> quadGeode = new osg::Geode;
		quadGeode->addDrawable(quad);
		quadGeode->getOrCreateStateSet()->setTextureAttributeAndModes(0, _scanner->getTexture());

		
		_viewer = new osgViewer::Viewer;
		_viewer->setUpViewInWindow(100, 100, windowSize.x(), windowSize.y());
		
		
		/*osgViewer::Viewer::Windows windows;
		_viewer->getWindows(windows);*/

		
		_viewer->getCamera()->setClearColor(osg::Vec4(1.0, 0, 0, 1));
		_viewer->getCamera()->setViewMatrix(osg::Matrix::identity());
		_viewer->getCamera()->setProjectionMatrixAsOrtho2D(0, windowSize.x(), 0, windowSize.y());
		_viewer->getCamera()->setClearMask(0x0);
		_viewer->getCamera()->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);
		_viewer->getCamera()->setViewport(0, 0, windowSize.x(), windowSize.y());

		_viewer->getCamera()->setRenderOrder(osg::Camera::POST_RENDER, 1);

		_viewer->getCamera()->addChild(_scanner);
		_viewer->getCamera()->addChild(quadGeode);

		//the frame processor must be attached to the camera rendering what the scanner sees
		//in order to be able to access updated framebufferobject data in compute shader
		//this is not really nice, some custom code may be better
		_viewer->getCamera()->addChild(_scanner->getFrameProcessor());

		_viewer->setRunFrameScheme(osgViewer::ViewerBase::FrameScheme::CONTINUOUS);
		_viewer->setThreadingModel(osgViewer::ViewerBase::ThreadingModel::SingleThreaded);

		_viewer->frame(); //eat a frame, first frame seems not executing shaders
		
		timeB = QDateTime::currentMSecsSinceEpoch();
		
		OSG_NOTICE << "first frame time (ms): " << timeB - timeA << std::endl;

	}

	void ScanProcess::loop()
	{
		if (_state == ABORTED)
			return;

		scannerLoop();
		if (_state == ABORTED)
			return;

		if (_appConfig.showResult){
			visualizationLoop();
		}
	}

	void ScanProcess::afterLoop()
	{
		_viewer->unref();
	}

	void ScanProcess::scannerLoop()
	{
		bool scanEnd = false;
		int c = 0;
		_viewer->getCamera()->setUpdateCallback(new itr::Scanner::UpdateCallback(_scanner));
		_viewer->getCamera()->setFinalDrawCallback(new FrameProcessor::FinalDrawCallBack(_scanner->getFrameProcessor()));
	
		changeProgress(0.0, "Scan start");

		ProgressWatcherThread  watcher(this);
		watcher.start();

		osg::Vec3 scannerStartPosition = _scanner->getPosition();

		int totalFrames = _frameCounter = 0;
		timeA = QDateTime::currentMSecsSinceEpoch();
		while (!scanEnd && !_viewer->done())
		{
			_viewer->frame();
			++_frameCounter;
			++totalFrames;

			double distanceCovered = (_scanner->getPosition() - scannerStartPosition).length();
			_progress = distanceCovered / _scanner->getScanLength() * 100.;

			if (_progress >= 100.0)
			{
				_scanner->getFrameProcessor()->setIsLastFrame(true);
				_viewer->frame();
				++_frameCounter;
				++totalFrames;
				scanEnd = true;
			}
		}

		timeB = QDateTime::currentMSecsSinceEpoch();


		OSG_NOTICE << "scan time (ms): " << timeB - timeA << std::endl;

		watcher.stop();
		watcher.wait();

		_viewer->getCamera()->setUpdateCallback(NULL);
		_viewer->getCamera()->setFinalDrawCallback(NULL);

		if (_viewer->done())
		{
			// process aborted by user
			changeState(ABORTED);
			return;
		}

		// we finish scanner computation
		// we now show reconstructed points
		changeProgress(_progress, "Scan end");
	}

	void ScanProcess::visualizationLoop()
	{
		changeProgress(_progress, "Showing point cloud (" + std::to_string(_scanner->getFrameProcessor()->getWorldPoints()->size()) + " points)");
		using namespace osg;
		ref_ptr<Vec3Array> normali = new Vec3Array;
		normali->push_back(Vec3(0, 0, 1));
		ref_ptr<Vec4Array> color = new Vec4Array;
		color->push_back(Vec4(1.0, 0.0, 0.0, 1.0));

		ref_ptr<Geometry> punti = new Geometry;
		punti->setVertexArray(_scanner->getFrameProcessor()->getWorldPoints());
		punti->setNormalArray(normali);
		punti->setColorArray(color);
		punti->setNormalBinding(Geometry::BIND_OVERALL);
		punti->setColorBinding(Geometry::BIND_OVERALL);
		punti->getOrCreateStateSet()->setAttribute(new osg::Point(1.0f), osg::StateAttribute::ON);
		punti->addPrimitiveSet(new DrawArrays(GL_POINTS, 0, _scanner->getFrameProcessor()->getWorldPoints()->size()));
		punti->setUseVertexBufferObjects(true);
		punti->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
		osg::ref_ptr<osg::Geode> pointCloud = new osg::Geode;
		pointCloud->addDrawable(punti);

		osg::Camera* c2 = new Camera();
		c2->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		c2->setGraphicsContext(_viewer->getCamera()->getGraphicsContext());
		c2->setViewport(_viewer->getCamera()->getViewport());

		_viewer->setCamera(c2);
		_viewer->getCamera()->setProjectionMatrixAsPerspective(90, 1.0, 10, 4000);

		_viewer->getCamera()->addChild(_model);
		_viewer->getCamera()->addChild(pointCloud);
		_viewer->getCamera()->setClearColor(osg::Vec4(0.0, 0.3, 0.4, 1.0));
		_viewer->getCamera()->setCullingActive(false);

		osgGA::TrackballManipulator* manipulator = new osgGA::TrackballManipulator;
		_viewer->setCameraManipulator(manipulator);
		_viewer->getCamera()->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_viewer->getCamera()->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);

		BoundingSphere modelBound = _model->getBound();

		double radius =
			modelBound.radius();
		manipulator->setHomePosition(modelBound.center() + osg::Vec3(0, -2 * radius, radius / 2.0), modelBound.center(), osg::Vec3(0, 0, 1));
		manipulator->home(0);

		// Handle the keyboard inputs of the viewer
		itr::utils::KeyState* sKeyState = new itr::utils::KeyState;
		sKeyState->keyPressed = false;
		sKeyState->keyPressedCallback = new SwitchResultShow(_resultShowMask);

		// Setup the keyboard event handler
		_viewer->addEventHandler(new itr::utils::ResultViewKeyboardHandler(sKeyState, 's'));
		while (!_viewer->done())
		{
			punti->setNodeMask(_resultShowMask & SHOW_POINTS);
			_model->setNodeMask(_resultShowMask & SHOW_MESH);
			_viewer->frame();

			QThread::msleep(30);
		}
	}

}