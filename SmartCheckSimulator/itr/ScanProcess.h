#pragma once

#include <QtCore/qthread.h>
#include <QtCore/QDateTime.h>
#include <QOpenGLWidget>
#include <osgViewer/CompositeViewer>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osgGA/TrackballManipulator>
#include <osg/Point>
#include "itr/ApplicationConfiguration.h"
#include "itr/Scanner.h"
#include "itr/FrameProcessor.h"
#include "itr/utils.h"

namespace itr
{

	class ProgressWatcherThread;

	/*
	class ScanProcess

	It is the graphic thread that computes the scansion and shows the results into
	an osgViewer window
	*/
	class ScanProcess : 
		public QThread
	{

		Q_OBJECT

	public:

		//----------------------------------------------------------------------

		// For memory info:
		//PROCESS_MEMORY_COUNTERS p_m_c;

		//----------------------------------------------------------------------

		typedef enum state {
			READY,
			RUNNING,
			ABORTED,
			ENDED,
		} State;

		typedef enum resultShowMask {
			SHOW_POINTS = 1,
			SHOW_MESH = 2,
			SHOW_MESH_AND_POINTS = 3,
		} ResultShowMask;

		ScanProcess(itr::ApplicationConfiguration appConfig);

		double getProgress()
		{
			return _progress;
		}

		uint getFrameCount()
		{
			return _frameCounter;
		}

		void resetFrameCount()
		{
			_frameCounter = 0;
		}

		State getState()
		{
			return _state;
		}

		void run();
		void abort();
		void toggleResultSwitch();

		/*
		beforeLoop()

		do init operations before viewer loop
		*/
		void beforeLoop();

		/*
		loop()

		render loop
		*/
		void loop();
		
		/*
		afterLoop()

		do final operations after viewer loop
		*/
		void afterLoop();

		/*
		scannerLoop()

		loop of the scan phase
		*/
		void scannerLoop();

		/*
		visualizationLoop()

		loop of the results visualization phase
		*/
		void visualizationLoop();

		osg::ref_ptr<osg::Vec3Array> getResultCloud()
		{
			if (_scanner)
			{
				return _scanner->getFrameProcessor()->getWorldPoints();
			}

			return NULL;
		}

	signals:

		// Methods for user interface update (state and progress bar)
		void progressUpdate(double progress, QString progressDescription);
		void stateUpdate(int previous, int current);

	private:

		itr::ApplicationConfiguration _appConfig;
		osg::ref_ptr<itr::Scanner> _scanner;
		osg::ref_ptr<osg::Node> _model;

		State _state;
		double _progress;
		QString _progressDescription;
		uint _frameCounter;

		void changeProgress(double progress, std::string progressDescription);
		void changeState(State newState);

		osg::ref_ptr<osgViewer::Viewer> _viewer;

		osg::Vec2i windowSize = osg::Vec2i(800, 600);

		int _resultShowMask = ResultShowMask::SHOW_MESH_AND_POINTS;
	};

	/*
	class ProgressWatcherThread

	this is a thread used to monitor the fps-rate of the target scan process
	*/
	class ProgressWatcherThread :
		public QThread
	{

		Q_OBJECT

	public:

		ProgressWatcherThread(ScanProcess* target)
		{
			_target = target;
		}

		void run()
		{
			uint timeA = QDateTime::currentMSecsSinceEpoch();
			uint fps = 0;
			while (!_stop)
			{
				uint timeElapsed = QDateTime::currentMSecsSinceEpoch() - timeA;
				if (timeElapsed >= 1000)
				{
					fps = _target->getFrameCount()*1000 / timeElapsed;
					_target->resetFrameCount();
					timeA = QDateTime::currentMSecsSinceEpoch();
				}

				emit _target->progressUpdate(_target->getProgress(), QString::fromStdString("Scanning: (" + std::to_string(fps) + " fps)"));
				QThread::msleep(100);
			}
		}

		void stop()
		{
			_stop = true;
		}

	private:

		ScanProcess* _target;
		bool _stop = false;	
	};

}