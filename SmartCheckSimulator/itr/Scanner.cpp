#include "Scanner.h"

namespace itr
{
	using namespace std;

	Scanner::Scanner(const Scanner::Settings settings) :
		RealCamera(settings.fundamentalMatrix, settings.sensorSize)
	{

		lognotice << "Creating scanner" << endl;
		
		setDirection(settings.direction);
		setLooking(settings.looking);
		setSpeed(settings.speed);
		setPosition(settings.startPosition);
		setScanLength(settings.scanLength);
		setLaserAngle(settings.laserAngle);
		setLaserDistance(settings.laserDistance);
		setLaserAperture(settings.laserAperture);
		setRois(settings.rois);
		setCameraFPS(settings.cameraFPS);
		setFocalLength(settings.focalLength);
		setScanMode(settings.scanMode);

		_brightnessThreshold = settings.brightnessThreshold;
		
		initialize();
	}

	Scanner::~Scanner()
	{
	}

	void Scanner::initialize()
	{
		loginfo << "initializing scanner" << this << endl;

		osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace(osg::CullFace::Mode::BACK);
		getOrCreateStateSet()->setAttributeAndModes(cullFace, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

		setViewMatrixAsLookAt(getPosition(), getPosition() + getLooking() * 100, osg::Vec3(0, 1, 0));
		setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);
		setRenderOrder(Camera::RenderOrder::NESTED_RENDER, 0);
	
		_laserColors[0] = osg::Vec4(1.0, 0.0, 0.0, 1.0);
		_laserColors[1] = osg::Vec4(0.0, 1.0, 0.0, 1.0);

		osg::Vec3 beamLimits[2];

		for (int i = 0; i < N_LASERS; ++i)
		{
			Laser::ScannerSide side = (i == 0 ? Laser::ScannerSide::SCANNER_FRONT_SIDE : Laser::ScannerSide::SCANNER_REAR_SIDE);
			lasers[i] = new Laser(this, osg::Vec3(0, 0, 0), beamLimits, side, laserDepthTextureSize);
			lasers[i]->setRenderOrder(Camera::RenderOrder::PRE_RENDER, 0);
			lasers[i]->setCullingMode(CullSettings::VIEW_FRUSTUM_CULLING);

			Camera::addChild(lasers[i]);
		}
		
		setupShader();
		
		_frameProcessor = new FrameProcessor(this);	
		_frameProcessor->setCullingActive(false);

		setClearColor(osg::Vec4(0.0, 0.0, 0.0, 1.0));
		//setComputeNearFarMode(osg::CullSettings::COMPUTE_NEAR_FAR_USING_BOUNDING_VOLUMES);

		bool needRefresh = true;
	}


	void Scanner::refresh()
	{
		loginfo << "refreshing scanner"<< this << endl;

		for (int i = 0; i < N_LASERS; ++i)
		{
			osg::Vec3 laserDirection;
			osg::Vec3 beamLimits[2];

			/*
			Lasers origins, the first has positive position relative to the scanner direction
			the second has negative position.
			Each of them has the same distance from the scanner (camera) position
			*/
			Laser::ScannerSide side = (i == 0 ? Laser::ScannerSide::SCANNER_FRONT_SIDE : Laser::ScannerSide::SCANNER_REAR_SIDE);

			laserDirection = -(_direction * side);

			double rotationAngle = getLaserAngle() * side;
			osg::Vec3 rotationAxis = _direction^_looking;
			rotationAxis.normalize();

			Laser::computeLaserBeamLimits(_laserAperture, rotationAngle, rotationAxis, -(_looking), laserDirection, beamLimits);
			lasers[i]->setBeamLimits(beamLimits);
			lasers[i]->setColor(_laserColors[i]);
		}

		needRefresh = false;
	}

	void Scanner::update(float deltaMillis)
	{
		if (needRefresh)
			refresh();

		_position += getDirection()*getSpeed()*deltaMillis / 1000.0f;

		osg::Vec3 eye = getPosition();
		osg::Vec3 lookat = eye + _looking * 10.0;
		osg::Vec3 up = _direction;

		setViewMatrix(osg::Matrix::lookAt(eye, lookat, up));

		// We need to compute near and far planes
		if (_model)
		{
			osg::BoundingSphere sceneBound = _model->getBound();

			// This may be changed to something more specific to our application //!!!
			if (sceneBound.valid())
			{
				double nearPlane = (getPosition() - sceneBound.center()).length() - sceneBound.radius();

				if (nearPlane < 10.0)
				{
					nearPlane = 10.0;
				}

				double farPlane = nearPlane + sceneBound.radius()*2.0;
				
				double fovy, ratio, previousNear, previousFar;

				getProjectionMatrixAsPerspective(fovy, ratio, previousNear, previousFar);

				setProjectionMatrixAsPerspective(fovy, ratio, nearPlane, farPlane);
			}
		}

		for (int i = 0; i < N_LASERS; i++)
		{
			lasers[i]->update(deltaMillis);
		}

		_frameProcessor->update();
	}

	void Scanner::setupShader()
	{
		osg::ref_ptr<osg::StateSet> state = getOrCreateStateSet();

		state->getOrCreateUniform("N_LASERS", osg::Uniform::INT)->set(N_LASERS);
		state->getOrCreateUniform("scanMode", osg::Uniform::UNSIGNED_INT)->set(getScanMode());		
		
		osg::ref_ptr<osg::Shader> vertexShader = new osg::Shader(osg::Shader::VERTEX);
		osg::ref_ptr<osg::Shader> fragmentShader = new osg::Shader(osg::Shader::FRAGMENT);

		/*
		string fileName = VERTEX_SCANNER_SHADER_FILE;

		bool success = vertexShader->loadShaderSourceFromFile(fileName);
		if (!success)
			logwarn << "Error loading shader: " << fileName << endl;
		fileName = FRAGMENT_SCANNER_SHADER_FILE;
		success = fragmentShader->loadShaderSourceFromFile(fileName);

		if (!success)
			logwarn << "Error loading shader: " << fileName << endl;
		*/

		//load shaders from shader header instead of files
		vertexShader = new osg::Shader(osg::Shader::VERTEX,itr::shaders::LASER_VERTEX_SHADER);
		fragmentShader = new osg::Shader(osg::Shader::FRAGMENT, itr::shaders::LASER_FRAGMENT_SHADER);
		

		osg::ref_ptr<osg::Program> shaderProgram = new osg::Program();

		shaderProgram->addShader(vertexShader);
		shaderProgram->addShader(fragmentShader);

		state->setAttributeAndModes(shaderProgram, osg::StateAttribute::ON);
	}

	bool Scanner::addChild(osg::ref_ptr<osg::Node> node)
	{
		using namespace osg;

		node->setDataVariance(Object::DataVariance::STATIC);
		/*	
		osg::ref_ptr<osg::Node> sceneOptimizedForLaser = dynamic_cast<osg::Node*>(node->clone(osg::CopyOp::DEEP_COPY_ALL));

		utils::divideGeometryIntoPrimitives(sceneOptimizedForLaser);
		
		utils::setUseVBO(sceneOptimizedForLaser);

		sceneOptimizedForLaser->setDataVariance(node->getDataVariance());
		sceneOptimizedForLaser->setStateSet(node->getStateSet());
		*/

		for (int i = 0; i < N_LASERS; i++)
		{
			lasers[i]->addChild(node);
		}
		Camera::addChild(node);

		_model = node;
		return true;
	}

	std::ostream &operator<<(std::ostream &os, const Scanner::Settings &config)
	{
		os << "direction: " << config.direction << endl;
		os << "looking: " << config.looking << endl;
		os << "speed: " << config.speed << endl;
		os << "start position: " << config.startPosition << endl;
		os << "scan length: " << config.scanLength << endl;
		os << "scan_mode: " << config.scanMode << endl;

		os << "laser distance: " << config.laserDistance << endl;
		os << "laser angle: " << config.laserAngle << endl;
		os << "laser aperture: " << config.laserAperture << endl;
		os << "focal_length: " << config.focalLength << endl;
		os << "sensor size: " << config.sensorSize << endl;
		os << "camera matrix: " << config.fundamentalMatrix << endl;
		os << "distortion coefficients: " << config.distortionCoefficients << endl;

		os << "brightness threshold: " << config.brightnessThreshold << std::endl;
		os << "camera fps: " << config.cameraFPS << std::endl;
		os << "rois: " << config.rois << endl;
		return os;
	}

	// serialization methods
	void Scanner::Settings::write(cv::FileStorage& fs) const
	{
		fs << "{";
		fs << "camera" << "{";
		fs << "matrix" << fundamentalMatrix;
		fs << "distortion_coefficients" << distortionCoefficients;
		fs << "focal_length" << focalLength;
		fs << "sensor_size" << sensorSize;
		fs << "}"; //camera

		std::vector<float> vec3;
		vec3.assign(startPosition._v, startPosition._v + 3);
		fs << "start_position" << vec3;
		fs << "speed" << speed;
		fs << "scan_length" << scanLength;

		vec3.assign(direction._v, direction._v + 3);
		fs << "direction" << vec3;

		vec3.assign(looking._v, looking._v + 3);
		fs << "looking" << vec3;

		fs << "scan_mode" << (int)scanMode;

		std::vector<int> vec4;
		vec4.assign(rois._v, rois._v + 4);

		fs << "rois" << vec4;
		fs << "brightness_threshold" << brightnessThreshold;
		fs << "camera_fps" << cameraFPS;

		fs << "lasers" << "{";
		fs << "distance" << laserDistance;
		fs << "aperture" << laserAperture;
		fs << "angle" << laserAngle;
		fs << "}";
	}

	void Scanner::Settings::read(const cv::FileNode& node)  //Read serialization for this class
	{

		std::vector<float> vec3;
		node["start_position"] >> vec3;
		startPosition = osg::Vec3(vec3[0], vec3[1], vec3[2]);

		node["speed"] >> speed;
		node["scan_length"] >> scanLength;
		node["direction"] >> vec3;
		direction = osg::Vec3(vec3[0], vec3[1], vec3[2]);

		
		node["looking"] >> vec3;
		looking = osg::Vec3(vec3[0], vec3[1], vec3[2]);

		std::vector<int> vec4;
		node["rois"] >> vec4;
		rois = osg::Vec4i(vec4[0], vec4[1], vec4[2], vec4[3]);

		node["brightness_threshold"] >> brightnessThreshold;
		node["camera_fps"] >> cameraFPS;
		int m;
		node["scan_mode"] >> m;
		this->scanMode = (ScanMode)m;

		cv::FileNode lasers = node["lasers"];

		lasers["distance"] >> laserDistance;
		lasers["aperture"] >> laserAperture;
		lasers["angle"] >> laserAngle;


		cv::FileNode camera = node["camera"];

		camera["matrix"] >> fundamentalMatrix;
		camera["sensor_size"] >> sensorSize;
		camera["distortion_coefficients"] >> distortionCoefficients;
		camera["focal_length"] >> focalLength;
	}

	bool Scanner::Settings::checkSensorSize()
	{
		if (this->sensorSize.width > 0 && this->sensorSize.height > 0) return true;
		return false;
	}

	bool Scanner::Settings::checkDistortionCoefficients()
	{
		for (int rowindex = 0; rowindex < this->distortionCoefficients.rows; ++rowindex) {
			for (int columnIndex = 0; columnIndex < this->distortionCoefficients.cols; ++columnIndex) {
				if (this->distortionCoefficients.at<float>(rowindex, columnIndex) < 0) return false;
			}
		}
		return true;
	}

	bool Scanner::Settings::checkCameraMatrix()
	{
		// not defined
		return true;
	}

	bool Scanner::Settings::checkFPS()
	{
		if (this->cameraFPS <= 0) return false;
		return true;
	}

	bool Scanner::Settings::checkInitialPosition()
	{
		// nothing to check
		return true;
	}

	bool Scanner::Settings::checkVelocity()
	{
		if (this->speed <= 0) return false;
		return true;
	}

	bool Scanner::Settings::checkScanLength()
	{
		if (this->scanLength <= 0) return false;
		return true;
	}

	bool Scanner::Settings::checkLasersDistance()
	{
		if (this->laserDistance <= 0) return false;
		return true;
	}

	bool Scanner::Settings::checkLasersAperture()
	{
		if (this->laserAperture <= 0 || this->laserAperture >= 180) return false;
		return true;
	}

	bool Scanner::Settings::checkLasersAngle()
	{
		if (this->laserAngle <= 0 || this->laserAngle >= 90) return false;
		return true;
	}

	bool Scanner::Settings::checkBrightnessThreshold()
	{
		if (this->brightnessThreshold < 0 || this->brightnessThreshold > 1) return false;
		return true;
	}

	bool Scanner::Settings::checkFocalLength()
	{
		if (this->focalLength <= 0.0) return false;
		return true;
	}

	bool Scanner::Settings::checkRois()
	{
		if (((this->rois[0] >= 0) && (this->rois[1] > 0) && (this->rois[2] > 0) && (this->rois[3] > 0))
			&&
			((this->rois[0] + this->rois[1]) <= (this->rois[2]))
			&&
			((this->rois[0] + this->rois[1]) <= (this->sensorSize.height))
			&&
			((this->rois[2] + this->rois[3]) <= (this->sensorSize.height))
			&&
			((this->rois[1] + this->rois[3]) <= (this->sensorSize.height))
			)
		{
			return true;
		}
		return false;
	}

	bool Scanner::Settings::validate()
	{
		_errors.clear();

		if (!checkSensorSize()) _errors.push_back("Invalid sensor size!");
		if (!checkDistortionCoefficients()) _errors.push_back("Invalid distortion coefficients!");
		if (!checkCameraMatrix()) _errors.push_back("Invalid camera matrix!");
		if (!checkFPS()) _errors.push_back("Invalid FPS value!");
		if (!checkInitialPosition()) _errors.push_back("Invalid initial position!");
		if (!checkVelocity()) _errors.push_back("Invalid velocity value!");
		if (!checkScanLength()) _errors.push_back("Invalid scan length!");
		if (!checkLasersDistance()) _errors.push_back("Invalid laser distance!");
		if (!checkLasersAperture()) _errors.push_back("invalid lasers aperture!");
		if (!checkLasersAngle()) _errors.push_back("Invalid lasers angle!");
		if (!checkBrightnessThreshold()) _errors.push_back("Invalid light threshold!");
		if (!checkRois()) _errors.push_back("Invalid rois!");
		if (!checkFocalLength()) _errors.push_back("Invalid focal length!");

		return _errors.empty();
	}

}