#pragma once

#include <iostream>
#include <opencv2/core/core.hpp>
#include <osg/Vec3>
#include <osg/CullFace>
#include "itr/utils.h"
#include "itr/Laser.h"
#include "itr/RealCamera.h"
#include "itr/FrameProcessor.h"
#include "itr/Serializable.h"
#include "shaders.h"

namespace itr
{

	class Laser;
	class FrameProcessor;

	/*
	class Scanner

	This class realizes the scanner object
	*/
	class Scanner :public RealCamera
	{

		const std::string VERTEX_SCANNER_SHADER_FILE = "shaders/shader.vert";
		const std::string FRAGMENT_SCANNER_SHADER_FILE = "shaders/shader.frag";

	public:

		/*
		class UpdateCallback

		This class realizes the movement of the scanner
		*/
		class UpdateCallback :
			public osg::Callback
		{

		public:

			UpdateCallback(Scanner* scanner)
			{
				_scanner = scanner;
			}

			inline virtual bool run(osg::Object* object, osg::Object* data)
			{
				_scanner->update(1000.0 / _scanner->getCameraFPS());
				return traverse(object, data);
			}

		private:
			Scanner* _scanner;
		};

	public:

		/*
		class Settings

		This class uses the configuration for the scanner construction
		*/
		class Settings :
			public Serializable
		{

		public:

			typedef enum scanMode : unsigned int
			{
				MODEL_UNDER_CROSS = 0x1,
				MODEL_OVER_CROSS = 0x2
			} ScanMode;

			osg::Vec3 direction = osg::Vec3(0., 1., 0.);
			osg::Vec3 looking = osg::Vec3(0., 0., -1.0);
			float speed = 100.;
			osg::Vec3 startPosition = osg::Vec3(0., 0., 0.);;
			float scanLength = 100.;
			float laserDistance = 100.0;
			float laserAngle = 60.;
			float laserAperture = 60.;
			osg::Vec4i rois = osg::Vec4i(0, 0, 0, 0);
			float brightnessThreshold = 0.5;
			cv::Size sensorSize;
			cv::Mat fundamentalMatrix = cv::Mat(3, 3, CV_32F, cv::Scalar(0));
			cv::Mat distortionCoefficients = cv::Mat(1, 5, CV_32F, cv::Scalar(0));
			float cameraFPS;
			float focalLength;
			ScanMode scanMode = ScanMode::MODEL_OVER_CROSS;

			friend std::ostream& operator<< (std::ostream &out, const Scanner::Settings &settings);

			/*
			* opencv Serializable methods implementation
			*/
			void write(cv::FileStorage& fs) const;
			void read(const cv::FileNode& node);

			inline const std::vector<std::string>& getErrors()
			{
				return _errors;
			}

			bool validate();


		private:

			/*
			Check that the dimensions of the sensor are setted greater than zero
			*/
			bool checkSensorSize();

			/*
			Check that all the distortion coefficients given are >= zero
			*/
			bool checkDistortionCoefficients();

			/*
			Check the camera matrix (not used)
			*/
			bool checkCameraMatrix();

			/*
			Check that the value of Frame Per Second given is > 0
			*/
			bool checkFPS();

			/*
			Check that the initial position of the scanner (not used)
			*/
			bool checkInitialPosition();

			/*
			Check that the scanner velocity given is >= 0
			*/
			bool checkVelocity();

			/*
			Check that the lenght of scansion is >= 0
			*/
			bool checkScanLength();

			/*
			Check that the distance given between the lasers and the central camera is greater than zero
			*/
			bool checkLasersDistance();

			/*
			Check that the aperture angle (in degrees) for the lasers is greater than zero and less than 180
			*/
			bool checkLasersAperture();

			/*
			Check that the tilt angle (in degrees) for the lasers is greater than zero and less than 90
			*/
			bool checkLasersAngle();

			/*
			Check that the brightness threshold value given is greater than (or equal to) 0 and less than (or equal to) 1
			*/
			bool checkBrightnessThreshold();

			/*
			Check if the focal lenght is greater than 0
			*/
			bool checkFocalLength();

			/*
			This method controls if the rois values inserted in the "rois" field of the configuration file are congruent with the sensor dimensions in the camera's "sensor_size" field
			We inspect than whether the rois are not overlapped and whether their area coverage is contained in the sensor area
			*/
			bool checkRois();

			std::vector<std::string> _errors;
		};


		Scanner(const Scanner::Settings settings);
		Scanner() : Scanner(Settings()){};
		~Scanner();

		void update(float deltaMillis);

		bool addChild(osg::ref_ptr<osg::Node> node);

		inline osg::Vec3 getDirection()
		{
			return _direction;
		}

		inline osg::Vec3 getLooking()
		{
			return _looking;
		}

		inline void setLooking(osg::Vec3 looking)
		{
			looking.normalize();
			_looking = looking;

			// calculate the direction ortoghonal vector on looking-direction plane
			float lookingOnDirectionProjectionLength = _looking*getDirection();

			osg::Vec3 lookingOnDirectionProjectionPoint = getDirection() * lookingOnDirectionProjectionLength;

			_looking = _looking - lookingOnDirectionProjectionPoint;
			_looking.normalize();

			needRefresh = true;
		}

		inline float getSpeed()
		{
			return _speed;
		}

		inline void setSpeed(float speed)
		{
			if (speed < 0.)
				return;

			_speed = speed;
		}

		inline float getScanLength()
		{
			return _scanLength;
		}


		inline void setScanLength(float scanLength)
		{
			if (scanLength < 0.)
				return;

			_scanLength = scanLength;
		}

		inline float getLaserDistance()
		{
			return _laserDistance;
		}

		inline void setLaserDistance(float laserDistance)
		{
			if (laserDistance < 0.)
				return;

			_laserDistance = laserDistance;
			needRefresh = true;
		}

		inline float getLaserAngle()
		{
			return _laserAngle;
		}

		inline void setLaserAngle(float laserAngle)
		{
			if (laserAngle < 0.)
				return;

			_laserAngle = laserAngle;
			needRefresh = true;
		}

		void setRois(osg::Vec4i rois)
		{
			this->_rois = rois;
			needRefresh = true;
		}

		osg::Vec4i getRois()
		{
			return _rois;
		}

		inline float getBrightnessThreshold()
		{
			return _brightnessThreshold;
		}

		inline float getLaserAperture()
		{
			return _laserAperture;
		}

		inline void setLaserAperture(float laserAperture)
		{
			if (laserAperture < 0.)
				return;

			_laserAperture = laserAperture;
			needRefresh = true;
		}


		inline void setPosition(osg::Vec3 position)
		{
			_position = position;
		}

		inline osg::Vec3 getPosition()
		{
			return _position;
		}

		inline osg::ref_ptr<Laser>* getLasers()
		{
			return lasers;
		}


		inline float getCameraFPS()
		{
			return _cameraFPS;
		}

		inline void setCameraFPS(float cameraFPS)
		{
			if (cameraFPS <= 0.)
				return;

			_cameraFPS = cameraFPS;
			needRefresh = true;
		}

		inline itr::FrameProcessor * getFrameProcessor()
		{
			return _frameProcessor;
		}


		inline float getFocalLength()
		{
			return _focalLenght;
		}

		inline void setFocalLength(float focalLength)
		{
			_focalLenght = focalLength;
		}


		inline void setScanMode(Settings::ScanMode scanMode)
		{
			_scanMode = scanMode;
			needRefresh = true;
		}

		inline Settings::ScanMode getScanMode()
		{
			return _scanMode;
		}

		// lasers number
		const int N_LASERS = 2;

	private:

		void initialize();
		void Scanner::refresh();

		inline void setDirection(osg::Vec3 direction)
		{
			direction.normalize();
			_direction = direction;

			needRefresh = true;
		}

		void setupShader();

		osg::Vec3 _direction;
		osg::Vec3 _looking;
		osg::Vec4i _rois;
		float _speed;
		float _scanLength;
		float _laserDistance;
		float _laserAngle;
		float _laserAperture;
		osg::Vec3 _position;
		float _brightnessThreshold;
		float _cameraFPS;
		float _focalLenght;
		Settings::ScanMode _scanMode;

		osg::ref_ptr<osg::Node> _model;

		osg::ref_ptr<itr::FrameProcessor> _frameProcessor;

		bool needRefresh;

		osg::ref_ptr<Laser> lasers[2];

		osg::Vec4 _laserColors[2];

		osg::Vec2i laserDepthTextureSize = osg::Vec2i(2000, 50);

	};

	std::ostream &operator<<(std::ostream &os, const Scanner::Settings &settings);
}