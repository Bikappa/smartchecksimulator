#include "Serializable.h"

void write(cv::FileStorage& fs, const std::string& allocator, const Serializable& x)
{
	x.write(fs);
}

void read(const cv::FileNode& node, Serializable& x, const Serializable& defaultValue)
{
	if (node.empty())
		x = defaultValue;
	else
		x.read(node);
}
