#pragma once

#include <opencv2/core/core.hpp>

/*
class Serializable

This class represents an object writable in a file and readable from it
*/
class Serializable
{

public:

	virtual void write(cv::FileStorage& fs) const = 0;
	virtual void read(const cv::FileNode& node) = 0;
	virtual ~Serializable() {}
};

void write(cv::FileStorage& fs, const std::string&, const Serializable& x);
void read(const cv::FileNode& node, Serializable& x, const Serializable& defaultValue);