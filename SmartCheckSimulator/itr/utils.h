#pragma once

#include <osg/Notify>
#include <osg/io_utils>
#include <osg/Geode>
#include <osg/Geometry>
#include <string>
#include <osgGA/GUIEventHandler>
#include <osgDB/fstream>

// For memory information:
#include <windows.h>
#include <Psapi.h>

#define loginfo osg::notify(osg::INFO)
#define lognotice osg::notify(osg::NOTICE)
#define logwarn osg::notify(osg::WARN)
#define logfatal osg::notify(osg::FATAL)

namespace itr
{

	/*
	class Utils

	This class contains some various methods and classes
	*/
	class utils
	{

	public:

		inline static void setUseVBOGeode(osg::Geode *geode)
		{

			for (unsigned int i = 0; i < geode->getNumDrawables(); i++)
			{
				osg::Drawable *drawable = geode->getDrawable(i);
				osg::Geometry *geom = dynamic_cast<osg::Geometry *> (drawable);

				geom->setUseVertexBufferObjects(true);
			}
		}

		inline static void setUseVBO(osg::Node *nd)
		{
			/// here you have found a group
			osg::Geode *geode = dynamic_cast<osg::Geode *> (nd);
			if (geode)
			{
				// analyse the geode. If it isn't a geode the dynamic cast gives NULL.
				setUseVBOGeode(geode);
			}
			else
			{
				osg::Group *gp = dynamic_cast<osg::Group *> (nd);
				if (gp)
				{
					osg::notify(osg::WARN) << "Group " << gp->getName() << std::endl;
					for (unsigned int ic = 0; ic < gp->getNumChildren(); ic++)
					{
						setUseVBO(gp->getChild(ic));
					}
				}
				else {
					osg::notify(osg::WARN) << "Unknown node " << nd << std::endl;
				}
			}
		}

		inline static  void divideGeometryIntoPrimitives(osg::Geode *geode)
		{
			using namespace osg;
			using namespace std;
			std::vector<Drawable *> nuove;

			for (unsigned int i = 0; i < geode->getNumDrawables(); i++)
			{

				Drawable *drawable = geode->getDrawable(i);
				Geometry *geometry = dynamic_cast<Geometry *> (drawable);

				ref_ptr<Vec3Array> vertici = dynamic_cast<Vec3Array*>(geometry->getVertexArray());
				ref_ptr<Vec3Array> normali = dynamic_cast<Vec3Array*>(geometry->getNormalArray());

				Geometry * currentGeometry = NULL;
				
				// Breaks the geometry in parts
				unsigned int nParti =  geometry->getNumPrimitiveSets();
				nParti = geometry->getNumPrimitiveSets() > nParti ? nParti : geometry->getNumPrimitiveSets();

				for (unsigned int ipr = 0; ipr < geometry->getNumPrimitiveSets(); ipr++)
				{

					if ((ipr % (geometry->getNumPrimitiveSets() / nParti)) == 0)
					{

						if (currentGeometry != NULL)
						{
							nuove.push_back(currentGeometry);
						}

						currentGeometry = new Geometry;
						currentGeometry->setVertexArray(vertici);
						currentGeometry->setNormalArray(normali);

						currentGeometry->setNormalBinding(geometry->getNormalBinding());
					}

					PrimitiveSet* prset = geometry->getPrimitiveSet(ipr);

					currentGeometry->addPrimitiveSet(prset);

				}
				nuove.push_back(currentGeometry);
				notify(WARN) << "There are " << geometry->getNumPrimitiveSets() << endl;
				geode->removeDrawable(drawable);
			}

			notify(WARN) << "Nuove sono " << nuove.size() << endl;
			for (int i = 0; i < nuove.size(); ++i)
			{
				geode->addDrawable(nuove[i]);
			}
		}

		inline static void divideGeometryIntoPrimitives(osg::Node *node)
		{
			using namespace osg;
			using namespace std;
			notify(WARN) << "Divide into primitive " << endl;

			Geode *geode = dynamic_cast<Geode *> (node);
			if (geode)
			{
				// analyse the geode. If it isn't a geode the dynamic cast gives NULL.
				notify(WARN) << "Geode " << geode->getName() << endl;
				divideGeometryIntoPrimitives(geode);
			}
			else
			{
				Group *gp = dynamic_cast<Group *> (node);
				if (gp)
				{
					notify(WARN) << "Group " << gp->getName() << endl;
					for (unsigned int ic = 0; ic < gp->getNumChildren(); ic++)
					{
						divideGeometryIntoPrimitives(gp->getChild(ic));
					}

				}
				else {
					notify(WARN) << "Unknown node " << node << std::endl;
				}
			}
		}

		///*
		//Simple method for saving the point cloud
		//*/
		//inline static bool savePCDFile(osg::ref_ptr<osg::Vec3Array> points, std::string filename)
		//{
		//	using namespace osg;
		//	using namespace std;

		//	ofstream pcdFile;
		//	pcdFile.open(filename);

		//	std::vector<char> vec(4096);
		//	pcdFile.rdbuf()->pubsetbuf(&vec.front(), vec.size());

		//	if (!pcdFile.is_open() || pcdFile.fail())
		//	{
		//		return false;
		//	}

		//	pcdFile << "# .PCD v0.7 - Point Cloud Data file format" << endl;
		//	pcdFile << "VERSION 0.7" << endl;
		//	pcdFile << "FIELDS x y z" << endl;
		//	pcdFile << "SIZE 4 4 4" << endl;
		//	pcdFile << "TYPE F F F" << endl;
		//	pcdFile << "COUNT 1 1 1" << endl;
		//	pcdFile << "WIDTH " << points->size() << endl;
		//	pcdFile << "HEIGHT 1" << endl;
		//	pcdFile << "VIEWPOINT 0 0 0 1 0 0 0" << endl;
		//	pcdFile << "POINTS " << points->size() << endl;
		//	pcdFile << "DATA ascii" << endl;
		//	int kk;
		//	for (kk = 0; kk < points->size(); kk++)
		//	{
		//		pcdFile << points->at(kk).x() << " " << points->at(kk).y() << " " << points->at(kk).z() <<"\n";
		//	}
		//	pcdFile.close();

		//	if (kk > 0) return true;
		//	else return false; // if 0 points added than return false
		//}

		/*
		Simple method for saving the point cloud
		
		fprintf is faster than ostream
		*/
		inline static bool savePCDFile(osg::ref_ptr<osg::Vec3Array> points, std::string filename)
		{
			using namespace osg;
			using namespace std;

			FILE * outputFile = NULL;
			outputFile = fopen(filename.c_str(), "w");

			if (!outputFile){
				OSG_NOTICE << "Error writing cloud to " << filename << std::endl;
				return false;
			}
			fprintf(outputFile, "# .PCD v0.7 - Point Cloud Data file format\n");
			fprintf(outputFile, "VERSION 0.7\n");
			fprintf(outputFile, "FIELDS x y z\n");
			fprintf(outputFile, "SIZE 4 4 4\n");
			fprintf(outputFile, "TYPE F F F\n");
			fprintf(outputFile, "COUNT 1 1 1\n");
			fprintf(outputFile, "WIDTH %d\n", points->size());
			fprintf(outputFile, "HEIGHT 1\n");
			fprintf(outputFile, "VIEWPOINT 0 0 0 1 0 0 0 \n");
			fprintf(outputFile, "POINTS %d\n", points->size());
			fprintf(outputFile, "DATA ascii\n");
			
			for (std::vector<Vec3>::iterator it = points->begin(); it != points->end(); ++it) {
				fprintf(outputFile, "%f %f %f\n", it->x(), it->y(), it->z());
			}
			
			fclose(outputFile);

			return true;
		}

		/*
		class KeyPressedCallback

		This class will be extended by another class that defines the action to be done
		*/
		class KeyPressedCallback{

		public:
			virtual void action(){};
		};

		/*
		class KeyState

		This class realizes the communication between the event handler and the update callback by
		saving the information on the interaction of the user with the keyboard
		//*/
		class KeyState
		{

		public:
			bool keyPressed;
			KeyPressedCallback* keyPressedCallback;
		};


		/*
		class ResultViewKeyboardHandler

		This class specifies the information to be saved when the user presses some keys of the keyboard
		*/
		class ResultViewKeyboardHandler : 
			public osgGA::GUIEventHandler
		{

		public:
			ResultViewKeyboardHandler(KeyState* ksr, int keyHandled)
			{
				lognotice << " Handler for key: " << keyHandled << std::endl;
				_keyHandled = keyHandled;
				_keyStateRef = ksr;
			}

			virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
			{

				bool handled = false;
				lognotice << " Key event: " << ea.getKey() << std::endl;

				if (ea.getKey() == _keyHandled){
					if (!_keyStateRef->keyPressed && ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN){

						_keyStateRef->keyPressed = true;
						lognotice << _keyHandled << " key pressed" << std::endl;

						if (_keyStateRef->keyPressedCallback)
						{
							_keyStateRef->keyPressedCallback->action();
						}

						handled = true;
					}
					else if (_keyStateRef->keyPressed && ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
					{
						_keyStateRef->keyPressed = false;
						lognotice << _keyHandled << " key up" << std::endl;
						handled = true;
					}
				}

				return handled;
			}

		protected:

			KeyState* _keyStateRef;
			int _keyHandled;
		};

	};

}