#include "configureview.h"
#include <QtWidgets/QApplication>

const std::string INI_INPUT_FILE = "Input.ini";
const std::string OUTPUT_CLOUD_FILE = "cloud.pcd";

int runInConsole(char * modelFilePath);
size_t getPeakRSS();

int main(int argc, char *argv[])
{

	long mainStartTime = QDateTime::currentMSecsSinceEpoch();
	osg::setNotifyLevel(osg::NotifySeverity::NOTICE);

	
	int r;
	if (argc > 1){
		//model file in command line arguments
		//run application without gui

		char * modelFilePath = argv[1];
		r = runInConsole(modelFilePath);
	}else{
		QApplication a(argc, argv);
		ConfigureView w;
		w.show();

		r = a.exec();
	}

	long mainFinishTime = QDateTime::currentMSecsSinceEpoch();

	OSG_NOTICE << "total time (ms): " << mainFinishTime - mainStartTime << std::endl;
	return r;
}

int runInConsole(char * modelFilePath){

	OSG_NOTICE << "Running in console" << std::endl;
	OSG_NOTICE << "Model file path: " << modelFilePath << std::endl;

	long loadConfigurationStartTime = QDateTime::currentMSecsSinceEpoch();

	itr::ApplicationConfiguration configuration;
	
	if (itr::ApplicationConfiguration::readFromINIFile(INI_INPUT_FILE, configuration) == -1){
		OSG_NOTICE << "Error occured while reading: " << INI_INPUT_FILE  << std::endl;
		return 1;
	}
	
	configuration.showResult = false;
	configuration.modelPath = modelFilePath;

	long loadConfigurationEndTime = QDateTime::currentMSecsSinceEpoch();


	long scanProcessStartTime = QDateTime::currentMSecsSinceEpoch();
	itr::ScanProcess* scanProcess = new itr::ScanProcess(configuration);

	scanProcess->start(QThread::Priority::TimeCriticalPriority);
	scanProcess->wait();

	long scanProcessEndTime = QDateTime::currentMSecsSinceEpoch();
	
	
	//write resulting point cloud
	long writePCDStartTime, writePCDEndTime;
	if (scanProcess->getState() == itr::ScanProcess::State::ENDED){
		OSG_NOTICE << "Saving cloud file..." << std::flush;
		writePCDStartTime = QDateTime::currentMSecsSinceEpoch();
		itr::utils::savePCDFile(scanProcess->getResultCloud(), OUTPUT_CLOUD_FILE);
		writePCDEndTime = QDateTime::currentMSecsSinceEpoch();
		OSG_NOTICE << "DONE" << std::endl;
	}
	else{
		OSG_NOTICE << "Something went wrong"<< std::endl;
		return 1;
	}

	OSG_NOTICE << "load configuration time (ms): " << loadConfigurationEndTime - loadConfigurationStartTime << std::endl;
	OSG_NOTICE << "scan process time = load model + first frame + scan  (ms): " << scanProcessEndTime - scanProcessStartTime << std::endl;
	OSG_NOTICE << "PCD writing time (ms): " << writePCDEndTime - writePCDStartTime << std::endl;

	OSG_NOTICE << "memory usage peak  (bytes):: " << getPeakRSS() << std::endl;
	return 0;
}



/**
* Returns the peak (maximum so far) resident set size (physical
* memory use) measured in bytes
*/
size_t getPeakRSS()
{
#if defined(_WIN32)
	/* Windows -------------------------------------------------- */
	PROCESS_MEMORY_COUNTERS info;
	GetProcessMemoryInfo(GetCurrentProcess(), &info, sizeof(info));
	return (size_t)info.PeakWorkingSetSize;
#endif
}