#version 430

layout (local_size_x = 64,local_size_y = 1, local_size_z = 2) in;


const float SAME_LIGHT_THRESHOLD = 0.005;
const float MAX_BRIGHTNESS = 1.0 - 0.001;

const uint MODEL_UNDER_CROSS = 0x1;
const uint MODEL_OVER_CROSS =  0x2;

layout(std430, binding = 0) coherent buffer reconstructedPoints
{
		int nPoints;
		float points[];
};

layout(rgba8) uniform readonly image2D targetImage;
uniform ivec4 rois;

uniform ivec2 imageSize;

 uniform float brightnessThreshold;
 uniform mat4 worldToPixelCoord;
 uniform mat4 pixelToWorldCoord;
 uniform vec3 laserOrigins[2];
 uniform vec3 laserNormals[2];
 uniform float focalLengthPC;
 uniform uint scanMode;

int compareLights(float target, float toCheck){

    if(abs(toCheck-target) <= SAME_LIGHT_THRESHOLD){
      return 0;
    }else if(toCheck < target){
      return -1;
    }else{
      return 1;
    }

}

bool linePlaneIntersection( vec3 planeNormal,  vec3 planePoint,  vec3 lineNormal,  vec3 linePoint, out vec3 intersectionPoint){
	 float d = dot(planeNormal, planePoint);

	 if (dot(planeNormal, lineNormal) == 0) {
		 return false; // No intersection, the ray is parallel to the plane
	 }
		
	 // compute the X value for the directed line ray intersecting the plane
	 float x = (d - dot(planeNormal, linePoint)) / dot(planeNormal, lineNormal);

	 // output contact point
	 intersectionPoint = linePoint + lineNormal * x; //Make sure your ray vector is normalized
	
	return  true;
 }


void main() {

	uint column = gl_GlobalInvocationID.x;

    if(column >= imageSize.x){
            return; //we are out of image;
    }
	uint roi = gl_GlobalInvocationID.z;

	uint ROI_OFFSET = roi*imageSize.x*2;

	// data[ROI_OFFSET + column * 2 ] = -1.0;
	// data[ROI_OFFSET + column * 2 + 1] = -1.0;

	int roiStart =	rois[roi*2];
	int	roiEnd = roiStart + rois[roi*2 + 1];

	float maxLight = -1.0;
	int lightStart =  -1;
	int lightLength = 0;

	float row0Light;
	bool inLight = false;

	for(int y = roiStart; y < roiEnd; ++y){
			
			int yPixelCoord = imageSize.y - 1 - y; //OPENGL images have row 0 at bottom

			vec4 pixel = imageLoad(targetImage, ivec2(column,yPixelCoord));
			float brightness = max(pixel.r, max(pixel.g, pixel.b));
			
			if(brightness > brightnessThreshold && compareLights(maxLight, brightness) == 1){
				lightStart = y;
				maxLight = brightness;
				lightLength = 1;
				inLight = true;
			}else if(inLight && compareLights(maxLight,brightness)== 0){
				maxLight = maxLight*float(lightLength) + brightness;
				maxLight = maxLight/ float(++lightLength);
			}else{
				inLight = false;
				if(maxLight >= MAX_BRIGHTNESS){
					//dont search anymore
					break;
				}
			}
		}

	
	bool modelIsUnderCross = (scanMode & MODEL_UNDER_CROSS) != 0x0;

	vec3 sensorCenterPC = vec3((float(imageSize.x) - 1.0)/2.0,(float(imageSize.y) - 1.0)/2.0,0.0);
	

	 uint laserIndex = modelIsUnderCross ? 1 - roi : roi;

	 vec3 laserOriginPC = (worldToPixelCoord * vec4(laserOrigins[laserIndex],1.0)).xyz;
	 vec3 laserNormalPC =  (worldToPixelCoord * vec4(laserNormals[laserIndex],1.0)  - worldToPixelCoord *vec4(0, 0, 0,1)).xyz;
	 laserNormalPC = normalize(laserNormalPC);
	  
	
	 //check brightness
	if(maxLight >= brightnessThreshold){
			
		vec2 pixel = vec2(column, float(lightStart) + float(lightLength - 1)/2.0);
		
		vec3 lineNormal = vec3(pixel.x, pixel.y, - focalLengthPC) - sensorCenterPC;
		lineNormal = normalize(lineNormal);
		vec3 intersectionPoint = vec3(0,0,0);
		
		bool intersect;
		intersect = linePlaneIntersection(laserNormalPC, laserOriginPC, lineNormal, sensorCenterPC, intersectionPoint);
		
		if(intersect){
			
			int pos = atomicAdd(nPoints,1);
			intersectionPoint = (pixelToWorldCoord*vec4(intersectionPoint,1.0)).xyz;
			
			points[pos*3]=intersectionPoint.x;
			points[pos*3 + 1 ]=intersectionPoint.y;
			points[pos*3 + 2 ]=intersectionPoint.z;
		}	
	}
		 
		 
	

}
