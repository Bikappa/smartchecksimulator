varying vec4 ShadowCoord[2];



struct laserProjector {
	vec3 origin;
	vec3 bound1;
	vec3 bound2;
	vec3 beamPlaneNormal;
	vec3 color;
	float lineSize;
	mat4 depthMVP;
	int depthMapTexture;
};
uniform laserProjector lasers[2];

uniform sampler2D ShadowMap[2];

uniform int N_LASERS;

varying vec4 fragmentWorldCoords;

void computeLaserLightAttenuation(in vec3 point, in int laserID, out float attenuation){

	//check if point is near the laser plane;
	float distance = abs(dot(lasers[laserID].beamPlaneNormal, point - lasers[laserID].origin));

	if (distance *2.0 > lasers[laserID].lineSize){
		//point is too far
		attenuation = 1.0f;
	}else{


		/*
		Check if point is in ray bounds
		*/
		float boundDist1, boundDist2;
		boundDist1 = dot(lasers[laserID].bound1, point - lasers[laserID].origin);
		boundDist2 = dot(lasers[laserID].bound2, point - lasers[laserID].origin);

		if(boundDist1 > 0 && boundDist2 > 0){
				attenuation = sqrt(distance*2.0/lasers[laserID].lineSize); // pow(distance*2.0/lasers[laserID].lineSize, 2);
		}else{
		attenuation = 1.0;
			}
		}

	attenuation = min(attenuation, 1.0);
}

void main()
{
	vec3 point = fragmentWorldCoords.xyz;

	//color of fragment before laser
	vec3 fragmentColor = vec3(gl_Color) *0.2;

	int shadow = 0;

	float attenuation;
	float distanceFromLight;
	vec4 ShadowCoordWDiv;
	for(int i = 0; i < 2; ++i){
		lasers[i].origin;

		computeLaserLightAttenuation(point, i, attenuation);

		ShadowCoordWDiv = ShadowCoord[i] / ShadowCoord[i].w;
		//for laser shadow
		distanceFromLight = texture(ShadowMap[i], ShadowCoordWDiv.st).z;

		if(1.0-attenuation > 0.0){

			if(ShadowCoord[i].w > 0.0)
					shadow = distanceFromLight < ShadowCoordWDiv.z - 0.001 ? 1 : 0;

			if(shadow == 1){
				attenuation = 1.0;
			}
		}

		fragmentColor += lasers[i].color*(1-attenuation);
		//float xc = (ShadowCoordWDiv.x > 1.0 ||  ShadowCoordWDiv.x < 0.0)? 0:ShadowCoordWDiv.x;
		//float yc = (ShadowCoordWDiv.y > 1.0 ||  ShadowCoordWDiv.y< 0.0)? 0:ShadowCoordWDiv.y;
		
		//fragmentColor = lasers[i].color*distanceFromLight;
		//fragmentColor = vec3(ShadowCoordWDiv.x, 0, 0);
	}


	gl_FragColor = vec4( fragmentColor, 1.0);

}
